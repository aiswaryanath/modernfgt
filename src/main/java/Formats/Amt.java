package Formats;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

public class Amt {
@JacksonXmlProperty(localName = "Ccy",isAttribute = true)
public String Ccy="INR";
@JacksonXmlText
String value;

public String getValue() {
	return value;
}
public void setValue(String value) {
	this.value = value;
}
}
