package Formats;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;

import ModernFGT.Utility;

public class BatchHeader {

	private String recordtype;
	private String Filler1;
	private String txntypecode;
	private String SundryInfo;
	private String ValueDate;
	private String Filler;
	private String ServiceClassCode;
	private String CompanyName;
	private String CompanyData;
	private String CompanyID;
	private String StandardClassCode;
	private String CompanyEntryDescription;
	public String getCompanyEntryDescription() {
		return CompanyEntryDescription;
	}

	public void setCompanyEntryDescription(String companyEntryDescription) {
		CompanyEntryDescription = companyEntryDescription;
	}

	private String CompanyDate;
	private String SettlementDate;
	private String OriginatorStatusCode;
	private String GoIdentification;
	private String BatchNumber;
	
	public String readbatchheaderdatacibc80(Row row1)
	{
		Utility u=new Utility();
	BatchHeader h=new BatchHeader();
	
	
	 h.setRecordtype((StringUtils.rightPad(row1.getCell(71).toString(),1)));
	 h.setFiller1(StringUtils.rightPad("",46));
		h.setTxntypecode((StringUtils.rightPad(row1.getCell(72).toString(),3)));	
		h.setSundryInfo((StringUtils.rightPad(row1.getCell(74).toString(),10)));
		h.setValueDate(u.getISODate(row1.getCell(224).toString()));
		h.setFiller(StringUtils.rightPad("",14));
		StringBuffer headerString=new StringBuffer(h.getRecordtype()).append(h.getFiller1()).append(h.getTxntypecode()).append(h.getSundryInfo()).append(h.getValueDate()).append(h.getFiller());
	    
		return headerString.toString();
	
	
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}


	public String getFiller1() {
		return Filler1;
	}

	public void setFiller1(String filler1) {
		Filler1 = filler1;
	}

	public String getRecordtype() {
		return recordtype;
	}


	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}


	public String getTxntypecode() {
		return txntypecode;
	}


	public void setTxntypecode(String txntypecode) {
		this.txntypecode = txntypecode;
	}


	public String getSundryInfo() {
		return SundryInfo;
	}


	public void setSundryInfo(String sundryInfo) {
		SundryInfo = sundryInfo;
	}


	public String getValueDate() {
		return ValueDate;
	}


	public void setValueDate(String valueDate) {
		ValueDate = valueDate;
	}


	public String getFiller() {
		return Filler;
	}


	public void setFiller(String filler) {
		Filler = filler;
	}

	public String readbatchheaderdataNACHAEFT(Row row1) {
		Utility u=new Utility();
		BatchHeader h=new BatchHeader();
		
		
		 	h.setRecordtype((StringUtils.rightPad(row1.getCell(71).toString(),1)));
		 	h.setServiceClassCode((StringUtils.rightPad(row1.getCell(76).toString(),3)));
			h.setCompanyName((StringUtils.rightPad(row1.getCell(77).toString(),16)));	
			h.setCompanyData((StringUtils.rightPad(row1.getCell(78).toString(),20)));
			h.setCompanyID((StringUtils.rightPad(row1.getCell(79).toString(),10)));	
			h.setStandardClassCode((StringUtils.rightPad(row1.getCell(72).toString(),3)));
			h.setCompanyEntryDescription((StringUtils.rightPad(row1.getCell(80).toString(),10)));
			h.setCompanyDate((StringUtils.rightPad("",6)));
			h.setValueDate(u.getISODate(row1.getCell(224).toString()));
			h.setSettlementDate((StringUtils.rightPad("",3)));
			h.setOriginatorStatusCode("1");
			h.setGoIdentification((StringUtils.rightPad(row1.getCell(82).toString(),8)));
			h.setBatchNumber((StringUtils.rightPad(row1.getCell(83).toString(),7)));
			StringBuffer headerString=new StringBuffer(h.getRecordtype()).append(h.getServiceClassCode()).
			append(h.getCompanyName()).append(h.getCompanyData()).append(h.getCompanyID()).append(h.getStandardClassCode()).
			append(h.getCompanyEntryDescription()).append(h.getCompanyDate()).append(h.getValueDate()).append(h.getSettlementDate())
			.append(h.getOriginatorStatusCode()).append(h.getGoIdentification()).append(h.getBatchNumber());
		    
			return headerString.toString();
			// TODO Auto-generated method stub
		
	}

	public String getServiceClassCode() {
		return ServiceClassCode;
	}

	public void setServiceClassCode(String serviceClassCode) {
		ServiceClassCode = serviceClassCode;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getCompanyData() {
		return CompanyData;
	}

	public void setCompanyData(String companyData) {
		CompanyData = companyData;
	}

	public String getCompanyID() {
		return CompanyID;
	}

	public void setCompanyID(String companyID) {
		CompanyID = companyID;
	}

	public String getStandardClassCode() {
		return StandardClassCode;
	}

	public void setStandardClassCode(String standardClassCode) {
		StandardClassCode = standardClassCode;
	}

	public String getCompanyDate() {
		return CompanyDate;
	}

	public void setCompanyDate(String companyDate) {
		CompanyDate = companyDate;
	}

	public String getSettlementDate() {
		return SettlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		SettlementDate = settlementDate;
	}

	public String getOriginatorStatusCode() {
		return OriginatorStatusCode;
	}

	public void setOriginatorStatusCode(String originatorStatusCode) {
		OriginatorStatusCode = originatorStatusCode;
	}

	public String getGoIdentification() {
		return GoIdentification;
	}

	public void setGoIdentification(String goIdentification) {
		GoIdentification = goIdentification;
	}

	public String getBatchNumber() {
		return BatchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		BatchNumber = batchNumber;
	}
	
	
	

}
