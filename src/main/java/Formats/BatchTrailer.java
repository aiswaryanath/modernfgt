package Formats;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;

import ModernFGT.Utility;

public class BatchTrailer {
	
	String Recordtype;
	String Txncode;
	String BatchEntryCount;
	String Reserved;
	String Filler;
	String EntryDollar;
	String Filler1;
	String Serviceclasscode;
	String EntryCount;
	String EntryHash;
	String totaldebitamount;
	String totalcreditamount;
	String Companyid;
	String MessageAuthentication;
	String OriginatingIdentification;
	String BatchNumber;
	

	public String getServiceclasscode() {
		return Serviceclasscode;
	}
	public void setServiceclasscode(String serviceclasscode) {
		Serviceclasscode = serviceclasscode;
	}
	public String getEntryCount() {
		return EntryCount;
	}
	public void setEntryCount(String entryCount) {
		EntryCount = entryCount;
	}
	public String getEntryHash() {
		return EntryHash;
	}
	public void setEntryHash(String entryHash) {
		EntryHash = entryHash;
	}
	public String getTotaldebitamount() {
		return totaldebitamount;
	}
	public void setTotaldebitamount(String totaldebitamount) {
		this.totaldebitamount = totaldebitamount;
	}
	public String getTotalcreditamount() {
		return totalcreditamount;
	}
	public void setTotalcreditamount(String totalcreditamount) {
		this.totalcreditamount = totalcreditamount;
	}
	public String getCompanyid() {
		return Companyid;
	}
	public void setCompanyid(String companyid) {
		Companyid = companyid;
	}
	public String getMessageAuthentication() {
		return MessageAuthentication;
	}
	public void setMessageAuthentication(String messageAuthentication) {
		MessageAuthentication = messageAuthentication;
	}
	public String getOriginatingIdentification() {
		return OriginatingIdentification;
	}
	public void setOriginatingIdentification(String originatingIdentification) {
		OriginatingIdentification = originatingIdentification;
	}
	public String getBatchNumber() {
		return BatchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		BatchNumber = batchNumber;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public String readbatchtrailerdatacibc80(Row row1)
	{

	BatchTrailer h=new BatchTrailer();

	 	h.setRecordtype((StringUtils.rightPad(row1.getCell(190).toString(),1)));
		h.setTxncode((StringUtils.rightPad(row1.getCell(191).toString(),3)));	
		h.setBatchEntryCount((StringUtils.leftPad(row1.getCell(192).toString(),6)));
		h.setReserved((StringUtils.rightPad(row1.getCell(193).toString(),10)));
		h.setFiller(StringUtils.rightPad("",20));
		h.setEntryDollar((StringUtils.leftPad(row1.getCell(194).toString(),12)));
		h.setFiller1(StringUtils.rightPad("",28));
		StringBuffer trailerString=new StringBuffer(h.getRecordtype()).append(h.getTxncode()).append(h.getBatchEntryCount())
				.append(h.getReserved()).append(h.getFiller()).append(h.getEntryDollar()).append(h.getFiller1());
	    
		return trailerString.toString();

	}
	
	public String readbatchtrailerdataNACHAEFT(Row row1) {
		BatchTrailer h=new BatchTrailer();
	 	h.setRecordtype((StringUtils.rightPad(row1.getCell(190).toString(),1)));
		h.setServiceclasscode((StringUtils.rightPad(row1.getCell(195).toString(),3)));	
		h.setEntryCount((StringUtils.leftPad(row1.getCell(192).toString(),6)));
		h.setEntryHash((StringUtils.leftPad(row1.getCell(196).toString(),10)));
		h.setTotaldebitamount((StringUtils.leftPad(row1.getCell(197).toString(),12)));
		h.setTotalcreditamount((StringUtils.leftPad(row1.getCell(198).toString(),12)));
		h.setCompanyid((StringUtils.leftPad(row1.getCell(199).toString(),10)));
		
		h.setFiller(StringUtils.rightPad("",19));
		h.setReserved((StringUtils.rightPad("",6)));
		h.setOriginatingIdentification((StringUtils.rightPad(row1.getCell(200).toString(),8)));
		h.setBatchNumber(StringUtils.rightPad(row1.getCell(202).toString(),7));
		StringBuffer trailerString=new StringBuffer(h.getRecordtype()).append(h.getServiceclasscode()).append(h.getEntryCount())
		.append(h.getEntryHash()).append(h.getTotaldebitamount()).append(h.getTotalcreditamount()).append(h.getCompanyid())
		.append(h.getFiller()).append(h.getReserved()).append(h.getOriginatingIdentification()).append(h.getBatchNumber());
	    
		return trailerString.toString();

	}


	public String getRecordtype() {
		return Recordtype;
	}


	public void setRecordtype(String recordtype) {
		Recordtype = recordtype;
	}


	public String getTxncode() {
		return Txncode;
	}


	public void setTxncode(String txncode) {
		Txncode = txncode;
	}


	public String getBatchEntryCount() {
		return BatchEntryCount;
	}


	public void setBatchEntryCount(String batchEntryCount) {
		BatchEntryCount = batchEntryCount;
	}


	public String getReserved() {
		return Reserved;
	}


	public void setReserved(String reserved) {
		Reserved = reserved;
	}


	public String getFiller() {
		return Filler;
	}


	public void setFiller(String filler) {
		Filler = filler;
	}


	public String getEntryDollar() {
		return EntryDollar;
	}


	public void setEntryDollar(String entryDollar) {
		EntryDollar = entryDollar;
	}


	public String getFiller1() {
		return Filler1;
	}


	public void setFiller1(String filler1) {
		Filler1 = filler1;
	}


}
