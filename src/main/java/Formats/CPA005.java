package Formats;

import java.util.HashMap;
import java.util.List;

import ModernFGT.TestcaseName;

public class CPA005 {
	String Filename;

	Header header;
	Transaction transaction;
	Trailer trailer;
	String finalstring;
	public String getFinalstring() {
		return finalstring;
	}

	public void setFinalstring(String finalstring) {
		this.finalstring = finalstring;
	}

	public  HashMap<String,String> data=new HashMap<String,String>();

	public HashMap<String, String> getData() {
		return data;
	}

	public void setData(HashMap<String, String> data) {
		this.data = data;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	public String getFilename() {
		return Filename;
	}

	public void setFilename(String filename) {
		Filename = filename;
	}





}
