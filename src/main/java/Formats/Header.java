package Formats;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.SystemOutLogger;

import ModernFGT.Utility;

public class Header {
	String header="";
	String HRecordId;
	String CIBC80_Filler1;
	String CIBC80_DDC;
	String CIBC80_Filler2;
	String CIBC80_Filler3;
	String InstitutionNo;
	String BranchNo;
	String AccountNo;
	String CIBC80_Filler4;
	String OriginatorShortname;
	String CIBC80_Filler5;
	String CIBC80_Filler6;
	String Hdestination;
	String OriginatorNumber;
	String FileCreationDate;
	String FileCreationNumber;
	String Currencyindicator;
	String FileLogicalrecordcount;
	String Filler;
	String ReservedArea;
	String OriginationcontrolData;
	String folderindicator;
	String prioritycode;
	String FileID;
	String RecordSize;
	String BlockingFactor;
	String Format;
	String ImmediateDestinationName;
	String Originame;
	String ReferenceCode;
	String FileCreationTime;
	





	public String readheaderdata(Row row1, CPA005 c)
	{
		Utility u=new Utility();
	Header h=new Header();
    h.setHRecordId((StringUtils.rightPad(row1.getCell(17).toString(),1)));
	h.setHdestination((StringUtils.rightPad(row1.getCell(18).toString(),4)));	
	h.setOriginatorNumber((StringUtils.rightPad(row1.getCell(19).toString(),10)));
	h.setFileCreationDate(u.getISODate(row1.getCell(223).toString()));
	h.setFileCreationNumber(u.getRandomnumber(4));
	h.setCurrencyindicator((StringUtils.rightPad(row1.getCell(27).toString(),3)));
    h.setFileLogicalrecordcount((StringUtils.rightPad(row1.getCell(28).toString(),9)));
    h.setFiller(StringUtils.rightPad("",1406));
    h.setReservedArea(StringUtils.rightPad("",20));
    h.setOriginationcontrolData(h.getOriginatorNumber()+h.getFileCreationNumber());
    h.setFolderindicator(folderindicator);
    c.setHeader(h);
    StringBuffer headerString=new StringBuffer(h.getHRecordId());
    headerString.append(h.getFileLogicalrecordcount()).append(h.getOriginatorNumber()).append(h.getFileCreationNumber()).append(h.getFileCreationDate()).append(h.getHdestination()).append(h.getReservedArea()).append(h.getCurrencyindicator()).append(h.getFiller());
    
	return headerString.toString();
	}
	
	public String readheaderdatacibc80(Row row1, CPA005 c)
	{
		Utility u=new Utility();
	Header h=new Header();
    h.setHRecordId((StringUtils.rightPad(row1.getCell(17).toString(),1)));
  
	h.setCIBC80_Filler1(StringUtils.rightPad("",2));	
	h.setHdestination((StringUtils.rightPad(row1.getCell(18).toString(),5)));
	h.setCIBC80_Filler2(StringUtils.rightPad("",5));
	h.setOriginatorNumber((StringUtils.rightPad(row1.getCell(19).toString(),10)));
	h.setFileCreationDate(u.getISODate(row1.getCell(223).toString()));
	h.setFileCreationNumber(u.getRandomnumber(4));
	h.setCIBC80_Filler3(StringUtils.rightPad("",1));
	h.setInstitutionNo((StringUtils.rightPad(row1.getCell(22).toString(),4)));
	h.setBranchNo((StringUtils.rightPad(row1.getCell(23).toString(),5)));
	h.setAccountNo((StringUtils.rightPad(row1.getCell(24).toString(),12)));
	h.setCIBC80_Filler4(StringUtils.rightPad("",2));
	h.setOriginatorShortname((StringUtils.rightPad(row1.getCell(25).toString(),15)));
	h.setCIBC80_Filler5(StringUtils.rightPad("",1));
	h.setCurrencyindicator((StringUtils.rightPad(row1.getCell(27).toString(),3)));
	h.setCIBC80_Filler6(StringUtils.rightPad("",4));
    h.setFolderindicator(folderindicator);
    c.setHeader(h);
    StringBuffer headerString=new StringBuffer(h.getHRecordId()).append(h.getCIBC80_Filler1()).append(h.getHdestination()).
    append(h.getCIBC80_Filler2()).
    append(h.getOriginatorNumber()).append(h.getFileCreationDate()).append(h.getFileCreationNumber()).append(h.getCIBC80_Filler3()).
    append(h.getInstitutionNo()).append(h.getBranchNo()).append(h.getAccountNo()).append(h.getCIBC80_Filler4()).append(h.getOriginatorShortname())
    .append(h.getCIBC80_Filler5()).append(h.getCurrencyindicator()).append(h.getCIBC80_Filler6());
    
	return headerString.toString();
	}


	public String getFileCreationTime() {
		return FileCreationTime;
	}

	public void setFileCreationTime(String fileCreationTime) {
		FileCreationTime = fileCreationTime;
	}

	public String getFiller() {
		return Filler;
	}

	public void setFiller(String filler) {
		Filler = filler;
	}

	public String getReservedArea() {
		return ReservedArea;
	}

	public void setReservedArea(String reservedArea) {
		ReservedArea = reservedArea;
	}



	public String getFolderindicator() {
		return folderindicator;
	}


	public void setFolderindicator(String folderindicator) {
		this.folderindicator = folderindicator;
	}

	public String getHRecordId() {
		return HRecordId;
	}

	public void setHRecordId(String hRecordId) {
		HRecordId = hRecordId;
	}

	public String getHdestination() {
		return Hdestination;
	}

	public void setHdestination(String hdestination) {
		Hdestination = hdestination;
	}

	public String getOriginatorNumber() {
		return OriginatorNumber;
	}

	public void setOriginatorNumber(String originatorNumber) {
		OriginatorNumber = originatorNumber;
	}

	public String getFileCreationDate() {
		return FileCreationDate;
	}

	public void setFileCreationDate(String fileCreationDate) {
		FileCreationDate = fileCreationDate;
	}

	public String getFileCreationNumber() {
		return FileCreationNumber;
	}

	public void setFileCreationNumber(String fileCreationNumber) {
		FileCreationNumber = fileCreationNumber;
	}

	public String getCurrencyindicator() {
		return Currencyindicator;
	}

	public void setCurrencyindicator(String currencyindicator) {
		Currencyindicator = currencyindicator;
	}

	public String getFileLogicalrecordcount() {
		return FileLogicalrecordcount;
	}

	public  void setFileLogicalrecordcount(String fileLogicalrecordcount) {
		FileLogicalrecordcount = fileLogicalrecordcount;
	}

	public String getOriginationcontrolData() {
		return OriginationcontrolData;
	}


	public void setOriginationcontrolData(String originationcontrolData) {
		OriginationcontrolData = originationcontrolData;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getCIBC80_Filler1() {
		return CIBC80_Filler1;
	}

	public void setCIBC80_Filler1(String cIBC80_Filler1) {
		CIBC80_Filler1 = cIBC80_Filler1;
	}

	public String getCIBC80_DDC() {
		return CIBC80_DDC;
	}

	public void setCIBC80_DDC(String cIBC80_DDC) {
		CIBC80_DDC = cIBC80_DDC;
	}

	public String getCIBC80_Filler2() {
		return CIBC80_Filler2;
	}

	public void setCIBC80_Filler2(String cIBC80_Filler2) {
		CIBC80_Filler2 = cIBC80_Filler2;
	}

	public String getCIBC80_Filler3() {
		return CIBC80_Filler3;
	}

	public void setCIBC80_Filler3(String cIBC80_Filler3) {
		CIBC80_Filler3 = cIBC80_Filler3;
	}

	public String getInstitutionNo() {
		return InstitutionNo;
	}

	public void setInstitutionNo(String institutionNo) {
		InstitutionNo = institutionNo;
	}

	public String getBranchNo() {
		return BranchNo;
	}

	public void setBranchNo(String branchNo) {
		BranchNo = branchNo;
	}

	public String getAccountNo() {
		return AccountNo;
	}

	public void setAccountNo(String accountNo) {
		AccountNo = accountNo;
	}

	public String getCIBC80_Filler4() {
		return CIBC80_Filler4;
	}

	public void setCIBC80_Filler4(String cIBC80_Filler4) {
		CIBC80_Filler4 = cIBC80_Filler4;
	}

	public String getOriginatorShortname() {
		return OriginatorShortname;
	}

	public void setOriginatorShortname(String originatorShortname) {
		OriginatorShortname = originatorShortname;
	}

	public String getCIBC80_Filler5() {
		return CIBC80_Filler5;
	}

	public void setCIBC80_Filler5(String cIBC80_Filler5) {
		CIBC80_Filler5 = cIBC80_Filler5;
	}

	public String getCIBC80_Filler6() {
		return CIBC80_Filler6;
	}

	public void setCIBC80_Filler6(String cIBC80_Filler6) {
		CIBC80_Filler6 = cIBC80_Filler6;
	}

	public String readheaderdataNACHAEFT(Row row1, CPA005 c) {

			Utility u=new Utility();
		Header h=new Header();
	    h.setHRecordId((StringUtils.rightPad(row1.getCell(17).toString(),1)));
	    h.setPrioritycode("01");	
		h.setHdestination((StringUtils.rightPad(row1.getCell(30).toString(),10)));
		h.setOriginatorNumber((StringUtils.rightPad(row1.getCell(19).toString(),10)));
		//System.out.println("Originator no.header:"+h.getOriginatorNumber());
		h.setFileCreationDate(u.getISODate(row1.getCell(223).toString()));
		h.setFileCreationTime(u.getDateFormat("HHmm"));
		h.setFileID(u.getAlphabhet(1));
		h.setRecordSize((StringUtils.rightPad(row1.getCell(33).toString(),3)));
		
		h.setBlockingFactor((StringUtils.rightPad(row1.getCell(35).toString(),2)));
	
		h.setFormat((StringUtils.rightPad(row1.getCell(36).toString(),1)));
	
		h.setImmediateDestinationName((StringUtils.rightPad(row1.getCell(37).toString(),23)));
	
		h.setOriginame((StringUtils.rightPad(row1.getCell(38).toString(),23)));
	
		h.setReferenceCode((StringUtils.rightPad("",8)));
		
	    StringBuffer headerString=new StringBuffer(h.getHRecordId()).append(h.getPrioritycode()).append(h.getHdestination())
	    .append(h.getOriginatorNumber()).append(h.getFileCreationDate()).append(h.getFileCreationTime()).append(h.getFileID()).
	    append(h.getRecordSize()).append(h.getBlockingFactor()).append(h.getFormat()).append(h.getImmediateDestinationName())
	    .append(h.getOriginame()).append(h.getReferenceCode());
	   // System.out.println(headerString.toString());
		return headerString.toString();
		}

	public String getPrioritycode() {
		return prioritycode;
	}

	public void setPrioritycode(String prioritycode) {
		this.prioritycode = prioritycode;
	}

	public String getFileID() {
		return FileID;
	}

	public void setFileID(String fileID) {
		FileID = fileID;
	}

	public String getRecordSize() {
		return RecordSize;
	}

	public void setRecordSize(String recordSize) {
		RecordSize = recordSize;
	}

	public String getBlockingFactor() {
		return BlockingFactor;
	}

	public void setBlockingFactor(String blockingFactor) {
		BlockingFactor = blockingFactor;
	}

	public String getFormat() {
		return Format;
	}

	public void setFormat(String format) {
		Format = format;
	}

	public String getImmediateDestinationName() {
		return ImmediateDestinationName;
	}

	public void setImmediateDestinationName(String immediateDestinationName) {
		ImmediateDestinationName = immediateDestinationName;
	}

	public String getOriginame() {
		return Originame;
	}

	public void setOriginame(String originame) {
		Originame = originame;
	}

	public String getReferenceCode() {
		return ReferenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		ReferenceCode = referenceCode;
	}

	

}
