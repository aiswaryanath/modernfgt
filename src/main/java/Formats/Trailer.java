package Formats;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;

public class Trailer {
	String Blockcount;
	String EntryCount;
	String EntryHash;
	String BatchCount;
	String DetailCount;
	String Filler;
	String LogicalRecordTypeId;
	String LogicalRecordCount;
	String OriginationControldata;
	String TotalValueDebitTxn;
	String TotalNumberDebitTxn;
	String TotalValueCreditTxn;
	String TotalNumberCreditTxn;
	String TotalValueErrorETxn;
	String TotalNumberErrorETxn;
	String TotalValueErrorFTxn;
	String TotalNumberErrorFTxn;
	
	public static String readTrailerdata(Row row1)
	{
		//System.out.println("current row no:"+row1.getRowNum());
		Trailer t= new Trailer();
		t.setLogicalRecordTypeId((StringUtils.rightPad(row1.getCell(203).toString(),1)));
		t.setLogicalRecordCount((StringUtils.rightPad(row1.getCell(205).toString(),9)));
		t.setOriginationControldata((StringUtils.rightPad(row1.getCell(206).toString(),14)));
		t.setTotalValueDebitTxn((StringUtils.leftPad(row1.getCell(207).toString(),14,'0')));
		t.setTotalNumberDebitTxn((StringUtils.leftPad(row1.getCell(208).toString(),8,'0')));
		t.setTotalValueCreditTxn((StringUtils.leftPad(row1.getCell(209).toString(),14,'0')));
		t.setTotalNumberCreditTxn((StringUtils.leftPad(row1.getCell(210).toString(),8,'0')));
		t.setTotalValueErrorETxn((StringUtils.leftPad(row1.getCell(211).toString(),14,'0')));
		t.setTotalNumberErrorETxn((StringUtils.leftPad(row1.getCell(212).toString(),8,'0')));
		t.setTotalValueErrorFTxn((StringUtils.leftPad(row1.getCell(213).toString(),14,'0')));
		t.setTotalNumberErrorFTxn((StringUtils.leftPad(row1.getCell(214).toString(),8,'0')));
		
		StringBuffer s=new StringBuffer(t.getLogicalRecordTypeId()).append(t.getLogicalRecordCount()).append(t.getOriginationControldata())
		.append(t.getTotalValueDebitTxn()).append(t.getTotalNumberDebitTxn()).append(t.getTotalValueCreditTxn())
		.append(t.getTotalNumberCreditTxn()).append(t.getTotalValueErrorETxn()).append(t.getTotalNumberErrorETxn())
		.append(t.getTotalValueErrorFTxn()).append(t.getTotalNumberErrorFTxn());
		
		return StringUtils.rightPad(s.toString(),1464);
	}

	
	public static String readFileTrailerdatacibc80(Row row1)
	{
		//System.out.println("current row no:"+row1.getRowNum());
		Trailer t= new Trailer();
		t.setLogicalRecordTypeId((StringUtils.rightPad(row1.getCell(203).toString(),1)));
		t.setBatchCount((StringUtils.rightPad(row1.getCell(204).toString(),6)));//batchcount
		t.setDetailCount((StringUtils.rightPad(row1.getCell(205).toString(),6)));
		t.setFiller((StringUtils.rightPad("",67)));
		StringBuffer s=new StringBuffer(t.getLogicalRecordTypeId()).append(t.getBatchCount()).append(t.getDetailCount())
		.append(t.getFiller());
		
		
		return s.toString();
	}
	
	public String readFileTrailerdataNACHAEFT(Row row1) {
		// TODO Auto-generated method stub
		Trailer t= new Trailer();
		t.setLogicalRecordTypeId((StringUtils.rightPad(row1.getCell(203).toString(),1)));
		t.setBatchCount((StringUtils.rightPad(row1.getCell(204).toString(),6)));//batchcount
		t.setBlockcount((StringUtils.rightPad(row1.getCell(215).toString(),6)));
		t.setEntryCount((StringUtils.rightPad(row1.getCell(205).toString(),8)));
		t.setEntryHash((StringUtils.rightPad(row1.getCell(216).toString(),10)));
		t.setTotalValueDebitTxn((StringUtils.leftPad(row1.getCell(217).toString(),12)));
		t.setTotalValueCreditTxn((StringUtils.leftPad(row1.getCell(218).toString(),12)));
		t.setFiller((StringUtils.rightPad("",39)));
		StringBuffer s=new StringBuffer(t.getLogicalRecordTypeId()).append(t.getBatchCount()).append(t.getBlockcount())
		.append(t.getEntryCount()).append(t.getEntryHash()).append(t.getTotalValueDebitTxn()).append(t.getTotalValueCreditTxn()).append(t.getFiller());
		
		
		return s.toString();
	}

	public String getLogicalRecordTypeId() {
		return LogicalRecordTypeId;
	}

	public void setLogicalRecordTypeId(String logicalRecordTypeId) {
		LogicalRecordTypeId = logicalRecordTypeId;
	}

	public String getLogicalRecordCount() {
		return LogicalRecordCount;
	}

	public void setLogicalRecordCount(String logicalRecordCount) {
		LogicalRecordCount = logicalRecordCount;
	}

	public String getOriginationControldata() {
		return OriginationControldata;
	}

	public void setOriginationControldata(String originationControldata) {
		OriginationControldata = originationControldata;
	}

	public String getTotalValueDebitTxn() {
		return TotalValueDebitTxn;
	}

	public void setTotalValueDebitTxn(String totalValueDebitTxn) {
		TotalValueDebitTxn = totalValueDebitTxn;
	}

	public String getTotalNumberDebitTxn() {
		return TotalNumberDebitTxn;
	}

	public void setTotalNumberDebitTxn(String totalNumberDebitTxn) {
		TotalNumberDebitTxn = totalNumberDebitTxn;
	}

	public String getTotalValueCreditTxn() {
		return TotalValueCreditTxn;
	}

	public void setTotalValueCreditTxn(String totalValueCreditTxn) {
		TotalValueCreditTxn = totalValueCreditTxn;
	}

	public String getTotalNumberCreditTxn() {
		return TotalNumberCreditTxn;
	}

	public void setTotalNumberCreditTxn(String totalNumberCreditTxn) {
		TotalNumberCreditTxn = totalNumberCreditTxn;
	}

	public String getTotalValueErrorETxn() {
		return TotalValueErrorETxn;
	}

	public void setTotalValueErrorETxn(String totalValueErrorETxn) {
		TotalValueErrorETxn = totalValueErrorETxn;
	}

	public String getBatchCount() {
		return BatchCount;
	}


	public void setBatchCount(String batchCount) {
		BatchCount = batchCount;
	}


	public String getDetailCount() {
		return DetailCount;
	}


	public void setDetailCount(String detailCount) {
		DetailCount = detailCount;
	}


	public String getFiller() {
		return Filler;
	}


	public void setFiller(String filler) {
		Filler = filler;
	}


	public String getTotalNumberErrorETxn() {
		return TotalNumberErrorETxn;
	}

	public void setTotalNumberErrorETxn(String totalNumberErrorETxn) {
		TotalNumberErrorETxn = totalNumberErrorETxn;
	}

	public String getTotalValueErrorFTxn() {
		return TotalValueErrorFTxn;
	}

	public void setTotalValueErrorFTxn(String totalValueErrorFTxn) {
		TotalValueErrorFTxn = totalValueErrorFTxn;
	}

	public String getTotalNumberErrorFTxn() {
		return TotalNumberErrorFTxn;
	}

	public void setTotalNumberErrorFTxn(String totalNumberErrorFTxn) {
		TotalNumberErrorFTxn = totalNumberErrorFTxn;
	}


	public String getBlockcount() {
		return Blockcount;
	}


	public void setBlockcount(String blockcount) {
		Blockcount = blockcount;
	}


	public String getEntryCount() {
		return EntryCount;
	}


	public void setEntryCount(String entryCount) {
		EntryCount = entryCount;
	}


	public String getEntryHash() {
		return EntryHash;
	}


	public void setEntryHash(String entryHash) {
		EntryHash = entryHash;
	}



	}
	
	
	
	
	
	
	
	
	
	
	
	


