package Formats;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ModernFGT.Utility;;
public class Transaction {
	String RecordTypeCode;
	String AddendaTypecode;
	String PaymentInfo;
	String AddendaSeqno;
	String EntryDetailSeqnumber;
	
	String PayeeAccnumber;
	String Amount;
	String Crossrefno;
	String PayeeName;
	String LogicalRecordTypeID;
	String LogicalRecordCount;
	String OriginationControlData;
	String TxnTypeCode;
	String DateDundsAvl;
	String BeneBankTranistNum;
	String ItemTracenumber;
	String StoredTxnType;
	String OriginatorsShortName;
	String OriginatorsLongName;
	String TransitNoReturns;
	String AccNoReturns;
	String OrigSettlCode;
	String InvalidDataElementID;
	String OriginatorsSundryInformation;
	String OrignDirectClearerUserID;
	String InstitutionIdentificationNumber;
	String Filler;
	String PayeeAccnumber2;
	String Amount2;
	String Crossrefno2;
	String PayeeName2;
	String TxnTypeCode2;
	String DateDundsAvl2;
	String BeneBankTranistNum2;
	String ItemTracenumber2;
	String StoredTxnType2;
	String OriginatorsShortName2;
	String OriginatorsLongName2;
	String TransitNoReturns2;
	String AccNoReturns2;
	String OrigSettlCode2;
	String InvalidDataElementID2;
	String OriginatorsSundryInformation2;
	String OrignDirectClearerUserID2;
	String InstitutionIdentificationNumber2;
	String Filler2;
	String TxnTypeCode3;
	String PayeeAccnumber3;
	String Amount3;
	String Crossrefno3;
	String PayeeName3;
	String DateDundsAvl3;
	String BeneBankTranistNum3;
	String ItemTracenumber3;
	String StoredTxnType3;
	String OriginatorsShortName3;
	String OriginatorsLongName3;
	String TransitNoReturns3;
	String AccNoReturns3;
	String OrigSettlCode3;
	String InvalidDataElementID3;
	String OriginatorsSundryInformation3;
	String OrignDirectClearerUserID3;
	String InstitutionIdentificationNumber3;
	String Filler3;
	String TxnTypeCode4;
	String PayeeAccnumber4;
	String Amount4;
	String Crossrefno4;
	String PayeeName4;
	String DateDundsAvl4;
	String BeneBankTranistNum4;
	String ItemTracenumber4;
	String StoredTxnType4;
	String OriginatorsShortName4;
	String OriginatorsLongName4;
	String TransitNoReturns4;
	String AccNoReturns4;
	String OrigSettlCode4;
	String InvalidDataElementID4;
	String OriginatorsSundryInformation4;
	String OrignDirectClearerUserID4;
	String InstitutionIdentificationNumber4;
	String Filler4;
	String TxnTypeCode5;
	String PayeeAccnumber5;
	String Amount5;
	String Crossrefno5;
	String PayeeName5;
	String DateDundsAvl5;
	String BeneBankTranistNum5;
	String ItemTracenumber5;
	String StoredTxnType5;
	String OriginatorsShortName5;
	String OriginatorsLongName5;
	String TransitNoReturns5;
	String AccNoReturns5;
	String OrigSettlCode5;
	String InvalidDataElementID5;
	String OriginatorsSundryInformation5;
	String OrignDirectClearerUserID5;
	String InstitutionIdentificationNumber5;
	String Filler5;
	String TxnTypeCode6;
	String PayeeAccnumber6;
	String Amount6;
	String Crossrefno6;
	String PayeeName6;
	String DateDundsAvl6;
	String BeneBankTranistNum6;
	String ItemTracenumber6;
	String StoredTxnType6;
	String OriginatorsShortName6;
	String OriginatorsLongName6;
	String TransitNoReturns6;
	String AccNoReturns6;
	String OrigSettlCode6;
	String InvalidDataElementID6;
	String OriginatorsSundryInformation6;
	String OrignDirectClearerUserID6;
	String InstitutionIdentificationNumber6;
	String Filler6;


	Utility u=new Utility();

	public String readtransactiondatacibc80(Row row1,CPA005 c) {
		// TODO Auto-generated method stub
		Transaction t= new Transaction();
		t.setLogicalRecordTypeID((StringUtils.rightPad(row1.getCell(93).toString(),1)));
		t.setLogicalRecordCount((StringUtils.rightPad(row1.getCell(94).toString(),1)));//creditdebitidetifier
		t.setFiller((StringUtils.rightPad("",1)));
		t.setInstitutionIdentificationNumber((StringUtils.rightPad(row1.getCell(95).toString(),9)));//institution id and branch transit number
		t.setPayeeAccnumber((StringUtils.rightPad(row1.getCell(96).toString(),12)));
		t.setFiller2((StringUtils.leftPad("",5)));
		t.setAmount((StringUtils.leftPad(row1.getCell(101).toString(),10)));
		t.setCrossrefno((StringUtils.rightPad(row1.getCell(102).toString(),13)));
		t.setPayeeName((StringUtils.rightPad(row1.getCell(104).toString(),22)));
		t.setFiller3((StringUtils.rightPad("",6)));
		StringBuffer s=new StringBuffer(t.getLogicalRecordTypeID()).append(t.getLogicalRecordCount()).append(t.getFiller()).
		append(t.getInstitutionIdentificationNumber()).append(t.getPayeeAccnumber()).append(t.getFiller2()).append(t.getAmount())
		.append(t.getCrossrefno()).append(t.getPayeeName()).append(t.getFiller3());
		
		
		return s.toString();
	}
	
	
	
	
	
	
	public String readtransactiondata(Row row1,CPA005 c) {
		// TODO Auto-generated method stub
		Transaction t= new Transaction();
		t.setLogicalRecordTypeID((StringUtils.rightPad(row1.getCell(93).toString(),1)));
		t.setLogicalRecordCount((StringUtils.rightPad(row1.getCell(105).toString(),9)));
		t.setOriginationControlData((StringUtils.rightPad(c.getHeader().getOriginationcontrolData(),14)));
		t.setTxnTypeCode((StringUtils.rightPad(row1.getCell(107).toString(),3)));
		t.setAmount((StringUtils.leftPad(row1.getCell(101).toString(),10,'0')));
		t.setDateDundsAvl(u.getISODate(row1.getCell(224).toString()));
		t.setBeneBankTranistNum((StringUtils.rightPad(row1.getCell(109).toString(),9)));
		t.setPayeeAccnumber((StringUtils.rightPad(row1.getCell(96).toString(),12)));
		//t.setItemTracenumber((StringUtils.leftPad("003201020"+c.getHeader().getFileCreationNumber()+u.getSeriesnumber(9),22)));
		t.setItemTracenumber((StringUtils.leftPad((row1.getCell(112).toString().substring(0,9))+c.getHeader().getFileCreationNumber()+u.getSeriesnumber(9),22)));
		//System.out.println("itemtraceno:"+t.getItemTracenumber());
		t.setStoredTxnType((StringUtils.leftPad(row1.getCell(114).toString(),3)));
		//System.out.println("storedtxn type:"+t.getStoredTxnType());
		t.setOriginatorsShortName((StringUtils.rightPad(row1.getCell(115).toString(),15)));
		//System.out.println("shortname :"+t.getOriginatorsShortName());
		t.setPayeeName((StringUtils.rightPad(row1.getCell(104).toString(),30)));
		t.setOriginatorsLongName((StringUtils.rightPad(row1.getCell(116).toString(),30)));
		t.setOrignDirectClearerUserID((StringUtils.rightPad(row1.getCell(118).toString(),10)));
		t.setCrossrefno((StringUtils.rightPad(row1.getCell(102).toString(),19)));
		t.setTransitNoReturns((StringUtils.rightPad(row1.getCell(119).toString(),9)));
		t.setAccNoReturns((StringUtils.rightPad(row1.getCell(120).toString(),12)));
		t.setOriginatorsSundryInformation((StringUtils.rightPad(row1.getCell(121).toString(),15)));
		t.setFiller((StringUtils.rightPad("",22)));
		t.setOrigSettlCode((StringUtils.rightPad(row1.getCell(122).toString(),2)));
		t.setInvalidDataElementID((StringUtils.rightPad(row1.getCell(123).toString(),11)));
		StringBuffer s=new StringBuffer(t.getLogicalRecordTypeID()).append(t.getLogicalRecordCount()).append(t.getOriginationControlData()).append(t.getTxnTypeCode()).append(t.getAmount())
				.append(t.getDateDundsAvl()).append(t.getBeneBankTranistNum()).append(t.getPayeeAccnumber()).append(t.getItemTracenumber())
		.append(t.getStoredTxnType()).append(t.getOriginatorsShortName()).append(t.getPayeeName()).append(t.getOriginatorsLongName()).append(t.getOrignDirectClearerUserID())
		.append(t.getCrossrefno()).append(t.getTransitNoReturns()).append(t.getAccNoReturns()).append(t.getOriginatorsSundryInformation())
		.append(t.getFiller()).append(t.getOrigSettlCode()).append(t.getInvalidDataElementID());
		
		try
		{
			//System.out.println("Rowwnum:"+row1.getRowNum());
		String segment2=(row1.getCell(225).toString()).substring(0,3);
		//System.out.println("2:"+segment2);
		if((segment2.length()>0))
		{
			t.setTxnTypeCode2((StringUtils.rightPad(segment2,3)));
			t.setAmount2((StringUtils.leftPad(row1.getCell(226).toString(),10,'0')));
			t.setDateDundsAvl2(u.getISODate(row1.getCell(224).toString()));
			t.setBeneBankTranistNum2((StringUtils.rightPad(row1.getCell(227).toString(),9)));
			t.setPayeeAccnumber2((StringUtils.rightPad(row1.getCell(228).toString(),12)));
			t.setItemTracenumber2((StringUtils.leftPad((row1.getCell(112).toString().substring(0,9))+c.getHeader().getFileCreationNumber()+u.getSeriesnumber(9),22)));
			t.setStoredTxnType2((StringUtils.leftPad(row1.getCell(229).toString(),3)));
			t.setOriginatorsShortName2((StringUtils.rightPad(row1.getCell(230).toString(),15)));
			t.setPayeeName2((StringUtils.rightPad(row1.getCell(231).toString(),30)));
			t.setOriginatorsLongName2((StringUtils.rightPad(row1.getCell(232).toString(),30)));
			t.setOrignDirectClearerUserID2((StringUtils.rightPad(row1.getCell(233).toString(),10)));
			t.setCrossrefno2((StringUtils.rightPad(row1.getCell(234).toString(),19)));
			t.setTransitNoReturns2((StringUtils.rightPad(row1.getCell(235).toString(),9)));
			t.setAccNoReturns2((StringUtils.rightPad(row1.getCell(236).toString(),12)));
			t.setOriginatorsSundryInformation2((StringUtils.rightPad(row1.getCell(237).toString(),15)));
			t.setFiller2((StringUtils.rightPad("",22)));
			t.setOrigSettlCode2((StringUtils.rightPad(row1.getCell(238).toString(),2)));
			t.setInvalidDataElementID2((StringUtils.rightPad(row1.getCell(239).toString(),11)));
			s.append(t.getTxnTypeCode2()).append(t.getAmount2())
					.append(t.getDateDundsAvl2()).append(t.getBeneBankTranistNum2()).append(t.getPayeeAccnumber2()).append(t.getItemTracenumber2())
			.append(t.getStoredTxnType2()).append(t.getOriginatorsShortName2()).append(t.getPayeeName2()).append(t.getOriginatorsLongName2()).append(t.getOrignDirectClearerUserID2())
			.append(t.getCrossrefno2()).append(t.getTransitNoReturns2()).append(t.getAccNoReturns2()).append(t.getOriginatorsSundryInformation2())
			.append(t.getFiller2()).append(t.getOrigSettlCode2()).append(t.getInvalidDataElementID2());
		}
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			//System.out.println("Issue with segment 2"+e.getLocalizedMessage());
		}
		try
		{
		String segment3=(row1.getCell(240).toString()).substring(0,3);;
		System.out.println("3:"+segment3);
		if((segment3.length()>0))
		{
			t.setTxnTypeCode3((StringUtils.rightPad(segment3,3)));
			t.setAmount3((StringUtils.leftPad(row1.getCell(241).toString(),10,'0')));
			t.setDateDundsAvl3(u.getISODate(row1.getCell(224).toString()));
			t.setBeneBankTranistNum3((StringUtils.rightPad(row1.getCell(242).toString(),9)));
			t.setPayeeAccnumber3((StringUtils.rightPad(row1.getCell(243).toString(),12)));
			t.setItemTracenumber3((StringUtils.leftPad((row1.getCell(112).toString().substring(0,9))+c.getHeader().getFileCreationNumber()+u.getSeriesnumber(9),22)));
			t.setStoredTxnType3((StringUtils.leftPad(row1.getCell(244).toString(),3)));
			t.setOriginatorsShortName3((StringUtils.rightPad(row1.getCell(245).toString(),15)));
			t.setPayeeName3((StringUtils.rightPad(row1.getCell(246).toString(),30)));
			t.setOriginatorsLongName3((StringUtils.rightPad(row1.getCell(247).toString(),30)));
			t.setOrignDirectClearerUserID3((StringUtils.rightPad(row1.getCell(248).toString(),10)));
			t.setCrossrefno3((StringUtils.rightPad(row1.getCell(249).toString(),19)));
			t.setTransitNoReturns3((StringUtils.rightPad(row1.getCell(250).toString(),9)));
			t.setAccNoReturns3((StringUtils.rightPad(row1.getCell(251).toString(),12)));
			t.setOriginatorsSundryInformation3((StringUtils.rightPad(row1.getCell(252).toString(),15)));
			t.setFiller3((StringUtils.rightPad("",22)));
			t.setOrigSettlCode3((StringUtils.rightPad(row1.getCell(253).toString(),2)));
			t.setInvalidDataElementID3((StringUtils.rightPad(row1.getCell(254).toString(),11)));
			s.append(t.getTxnTypeCode3()).append(t.getAmount3())
					.append(t.getDateDundsAvl3()).append(t.getBeneBankTranistNum3()).append(t.getPayeeAccnumber3()).append(t.getItemTracenumber3())
			.append(t.getStoredTxnType3()).append(t.getOriginatorsShortName3()).append(t.getPayeeName3()).append(t.getOriginatorsLongName3()).append(t.getOrignDirectClearerUserID3())
			.append(t.getCrossrefno3()).append(t.getTransitNoReturns3()).append(t.getAccNoReturns3()).append(t.getOriginatorsSundryInformation3())
			.append(t.getFiller3()).append(t.getOrigSettlCode3()).append(t.getInvalidDataElementID3());
		}
		}
		catch(Exception e)
		{
			//System.out.println("Issue with segment 2");
		}
		try
		{
		String segment4=(row1.getCell(255).toString()).substring(0,3);;
		if((segment4.length()>0))
		{
			t.setTxnTypeCode4((StringUtils.rightPad(segment4,3)));
			t.setAmount4((StringUtils.leftPad(row1.getCell(256).toString(),10,'0')));
			t.setDateDundsAvl4(u.getISODate(row1.getCell(224).toString()));
			t.setBeneBankTranistNum4((StringUtils.rightPad(row1.getCell(257).toString(),9)));
			t.setPayeeAccnumber4((StringUtils.rightPad(row1.getCell(258).toString(),12)));
			t.setItemTracenumber4((StringUtils.leftPad((row1.getCell(112).toString().substring(0,9))+c.getHeader().getFileCreationNumber()+u.getSeriesnumber(9),22)));
			t.setStoredTxnType4((StringUtils.leftPad(row1.getCell(259).toString(),3)));
			t.setOriginatorsShortName4((StringUtils.rightPad(row1.getCell(260).toString(),15)));
			t.setPayeeName4((StringUtils.rightPad(row1.getCell(261).toString(),30)));
			t.setOriginatorsLongName4((StringUtils.rightPad(row1.getCell(262).toString(),30)));
			t.setOrignDirectClearerUserID4((StringUtils.rightPad(row1.getCell(263).toString(),10)));
			t.setCrossrefno4((StringUtils.rightPad(row1.getCell(264).toString(),19)));
			t.setTransitNoReturns4((StringUtils.rightPad(row1.getCell(265).toString(),9)));
			t.setAccNoReturns4((StringUtils.rightPad(row1.getCell(266).toString(),12)));
			t.setOriginatorsSundryInformation4((StringUtils.rightPad(row1.getCell(267).toString(),15)));
			t.setFiller4((StringUtils.rightPad("",22)));
			t.setOrigSettlCode4((StringUtils.rightPad(row1.getCell(268).toString(),2)));
			t.setInvalidDataElementID4((StringUtils.rightPad(row1.getCell(269).toString(),11)));
			s.append(t.getTxnTypeCode4()).append(t.getAmount4())
			.append(t.getDateDundsAvl4()).append(t.getBeneBankTranistNum4()).append(t.getPayeeAccnumber4()).append(t.getItemTracenumber4())
			.append(t.getStoredTxnType4()).append(t.getOriginatorsShortName4()).append(t.getPayeeName4()).append(t.getOriginatorsLongName4())
			.append(t.getOrignDirectClearerUserID4()).append(t.getCrossrefno4()).append(t.getTransitNoReturns4()).append(t.getAccNoReturns4())
			.append(t.getOriginatorsSundryInformation4()).append(t.getFiller4()).append(t.getOrigSettlCode4()).append(t.getInvalidDataElementID4());
		}
		}
		catch(Exception e)
		{
			//System.out.println("Issue with segment 2");
		}
		try
		{
		String segment5=(row1.getCell(270).toString()).substring(0,3);
		if((segment5.length()>0))
		{
			t.setTxnTypeCode5((StringUtils.rightPad(segment5,3)));
			t.setAmount5((StringUtils.leftPad(row1.getCell(271).toString(),10,'0')));
			t.setDateDundsAvl5(u.getISODate(row1.getCell(224).toString()));
			t.setBeneBankTranistNum5((StringUtils.rightPad(row1.getCell(272).toString(),9)));
			t.setPayeeAccnumber5((StringUtils.rightPad(row1.getCell(273).toString(),12)));
			t.setItemTracenumber5((StringUtils.leftPad((row1.getCell(112).toString().substring(0,9))+c.getHeader().getFileCreationNumber()+u.getSeriesnumber(9),22)));
			t.setStoredTxnType5((StringUtils.leftPad(row1.getCell(274).toString(),3)));
			t.setOriginatorsShortName5((StringUtils.rightPad(row1.getCell(275).toString(),15)));
			t.setPayeeName5((StringUtils.rightPad(row1.getCell(276).toString(),30)));
			t.setOriginatorsLongName5((StringUtils.rightPad(row1.getCell(277).toString(),30)));
			t.setOrignDirectClearerUserID5((StringUtils.rightPad(row1.getCell(278).toString(),10)));
			t.setCrossrefno5((StringUtils.rightPad(row1.getCell(279).toString(),19)));
			t.setTransitNoReturns5((StringUtils.rightPad(row1.getCell(280).toString(),9)));
			t.setAccNoReturns5((StringUtils.rightPad(row1.getCell(281).toString(),12)));
			t.setOriginatorsSundryInformation5((StringUtils.rightPad(row1.getCell(282).toString(),15)));
			t.setFiller5((StringUtils.rightPad("",22)));
			t.setOrigSettlCode5((StringUtils.rightPad(row1.getCell(283).toString(),2)));
			t.setInvalidDataElementID5((StringUtils.rightPad(row1.getCell(284).toString(),11)));
			s.append(t.getTxnTypeCode5()).append(t.getAmount5())
			.append(t.getDateDundsAvl5()).append(t.getBeneBankTranistNum5()).append(t.getPayeeAccnumber5()).append(t.getItemTracenumber5())
			.append(t.getStoredTxnType5()).append(t.getOriginatorsShortName5()).append(t.getPayeeName5()).append(t.getOriginatorsLongName5())
			.append(t.getOrignDirectClearerUserID5()).append(t.getCrossrefno5()).append(t.getTransitNoReturns5()).append(t.getAccNoReturns5())
			.append(t.getOriginatorsSundryInformation5()).append(t.getFiller5()).append(t.getOrigSettlCode5()).append(t.getInvalidDataElementID5());
		}
		}
		catch(Exception e)
		{
		//	System.out.println("Issue with segment 2");
		}
		try
		{
		String segment6=(row1.getCell(285).toString()).substring(0,3);;
		if((segment6.length()>0))
		{
			t.setTxnTypeCode6((StringUtils.rightPad(segment6,3)));
			t.setAmount6((StringUtils.leftPad(row1.getCell(286).toString(),10,'0')));
			t.setDateDundsAvl6(u.getISODate(row1.getCell(224).toString()));
			t.setBeneBankTranistNum6((StringUtils.rightPad(row1.getCell(287).toString(),9)));
			t.setPayeeAccnumber6((StringUtils.rightPad(row1.getCell(288).toString(),12)));
			t.setItemTracenumber6((StringUtils.leftPad((row1.getCell(112).toString().substring(0,9))+c.getHeader().getFileCreationNumber()+u.getSeriesnumber(9),22)));
			t.setStoredTxnType6((StringUtils.leftPad(row1.getCell(289).toString(),3)));
			t.setOriginatorsShortName6((StringUtils.rightPad(row1.getCell(290).toString(),15)));
			t.setPayeeName6((StringUtils.rightPad(row1.getCell(291).toString(),30)));
			t.setOriginatorsLongName6((StringUtils.rightPad(row1.getCell(292).toString(),30)));
			t.setOrignDirectClearerUserID6((StringUtils.rightPad(row1.getCell(293).toString(),10)));
			t.setCrossrefno6((StringUtils.rightPad(row1.getCell(294).toString(),19)));
			t.setTransitNoReturns6((StringUtils.rightPad(row1.getCell(295).toString(),9)));
			t.setAccNoReturns6((StringUtils.rightPad(row1.getCell(296).toString(),12)));
			t.setOriginatorsSundryInformation6((StringUtils.rightPad(row1.getCell(297).toString(),15)));
			t.setFiller6((StringUtils.rightPad("",22)));
			t.setOrigSettlCode6((StringUtils.rightPad(row1.getCell(298).toString(),2)));
			t.setInvalidDataElementID6((StringUtils.rightPad(row1.getCell(299).toString(),11)));
			s.append(t.getTxnTypeCode6()).append(t.getAmount6())
			.append(t.getDateDundsAvl6()).append(t.getBeneBankTranistNum6()).append(t.getPayeeAccnumber6()).append(t.getItemTracenumber6())
			.append(t.getStoredTxnType6()).append(t.getOriginatorsShortName6()).append(t.getPayeeName6()).append(t.getOriginatorsLongName6())
			.append(t.getOrignDirectClearerUserID6()).append(t.getCrossrefno6()).append(t.getTransitNoReturns6()).append(t.getAccNoReturns6())
			.append(t.getOriginatorsSundryInformation6()).append(t.getFiller6()).append(t.getOrigSettlCode6()).append(t.getInvalidDataElementID6());
		}
		}
		catch(Exception e)
		{
			//System.out.println("Issue with segment 2");
		}
		return (StringUtils.rightPad(s.toString(),1464));
	}


	public String getTxnTypeCode2() {
		return TxnTypeCode2;
	}


	public void setTxnTypeCode2(String txnTypeCode2) {
		TxnTypeCode2 = txnTypeCode2;
	}


	public String getDateDundsAvl2() {
		return DateDundsAvl2;
	}


	public void setDateDundsAvl2(String dateDundsAvl2) {
		DateDundsAvl2 = dateDundsAvl2;
	}


	public String getBeneBankTranistNum2() {
		return BeneBankTranistNum2;
	}


	public void setBeneBankTranistNum2(String beneBankTranistNum2) {
		BeneBankTranistNum2 = beneBankTranistNum2;
	}


	public String getItemTracenumber2() {
		return ItemTracenumber2;
	}


	public void setItemTracenumber2(String itemTracenumber2) {
		ItemTracenumber2 = itemTracenumber2;
	}


	public String getStoredTxnType2() {
		return StoredTxnType2;
	}


	public void setStoredTxnType2(String storedTxnType2) {
		StoredTxnType2 = storedTxnType2;
	}


	public String getOriginatorsShortName2() {
		return OriginatorsShortName2;
	}


	public void setOriginatorsShortName2(String originatorsShortName2) {
		OriginatorsShortName2 = originatorsShortName2;
	}


	public String getOriginatorsLongName2() {
		return OriginatorsLongName2;
	}


	public void setOriginatorsLongName2(String originatorsLongName2) {
		OriginatorsLongName2 = originatorsLongName2;
	}


	public String getTransitNoReturns2() {
		return TransitNoReturns2;
	}


	public void setTransitNoReturns2(String transitNoReturns2) {
		TransitNoReturns2 = transitNoReturns2;
	}


	public String getAccNoReturns2() {
		return AccNoReturns2;
	}


	public void setAccNoReturns2(String accNoReturns2) {
		AccNoReturns2 = accNoReturns2;
	}


	public String getOrigSettlCode2() {
		return OrigSettlCode2;
	}


	public void setOrigSettlCode2(String origSettlCode2) {
		OrigSettlCode2 = origSettlCode2;
	}


	public String getInvalidDataElementID2() {
		return InvalidDataElementID2;
	}


	public void setInvalidDataElementID2(String invalidDataElementID2) {
		InvalidDataElementID2 = invalidDataElementID2;
	}


	public String getOriginatorsSundryInformation2() {
		return OriginatorsSundryInformation2;
	}


	public void setOriginatorsSundryInformation2(String originatorsSundryInformation2) {
		OriginatorsSundryInformation2 = originatorsSundryInformation2;
	}


	public String getOrignDirectClearerUserID2() {
		return OrignDirectClearerUserID2;
	}


	public void setOrignDirectClearerUserID2(String orignDirectClearerUserID2) {
		OrignDirectClearerUserID2 = orignDirectClearerUserID2;
	}


	public String getInstitutionIdentificationNumber2() {
		return InstitutionIdentificationNumber2;
	}


	public void setInstitutionIdentificationNumber2(String institutionIdentificationNumber2) {
		InstitutionIdentificationNumber2 = institutionIdentificationNumber2;
	}


	public String getFiller2() {
		return Filler2;
	}


	public void setFiller2(String filler2) {
		Filler2 = filler2;
	}


	public String getTxnTypeCode3() {
		return TxnTypeCode3;
	}


	public void setTxnTypeCode3(String txnTypeCode3) {
		TxnTypeCode3 = txnTypeCode3;
	}


	public String getDateDundsAvl3() {
		return DateDundsAvl3;
	}


	public void setDateDundsAvl3(String dateDundsAvl3) {
		DateDundsAvl3 = dateDundsAvl3;
	}


	public String getBeneBankTranistNum3() {
		return BeneBankTranistNum3;
	}


	public void setBeneBankTranistNum3(String beneBankTranistNum3) {
		BeneBankTranistNum3 = beneBankTranistNum3;
	}


	public String getItemTracenumber3() {
		return ItemTracenumber3;
	}


	public void setItemTracenumber3(String itemTracenumber3) {
		ItemTracenumber3 = itemTracenumber3;
	}


	public String getStoredTxnType3() {
		return StoredTxnType3;
	}


	public void setStoredTxnType3(String storedTxnType3) {
		StoredTxnType3 = storedTxnType3;
	}


	public String getOriginatorsShortName3() {
		return OriginatorsShortName3;
	}


	public void setOriginatorsShortName3(String originatorsShortName3) {
		OriginatorsShortName3 = originatorsShortName3;
	}


	public String getOriginatorsLongName3() {
		return OriginatorsLongName3;
	}


	public void setOriginatorsLongName3(String originatorsLongName3) {
		OriginatorsLongName3 = originatorsLongName3;
	}


	public String getTransitNoReturns3() {
		return TransitNoReturns3;
	}


	public void setTransitNoReturns3(String transitNoReturns3) {
		TransitNoReturns3 = transitNoReturns3;
	}


	public String getAccNoReturns3() {
		return AccNoReturns3;
	}


	public void setAccNoReturns3(String accNoReturns3) {
		AccNoReturns3 = accNoReturns3;
	}


	public String getOrigSettlCode3() {
		return OrigSettlCode3;
	}


	public void setOrigSettlCode3(String origSettlCode3) {
		OrigSettlCode3 = origSettlCode3;
	}


	public String getInvalidDataElementID3() {
		return InvalidDataElementID3;
	}


	public void setInvalidDataElementID3(String invalidDataElementID3) {
		InvalidDataElementID3 = invalidDataElementID3;
	}


	public String getOriginatorsSundryInformation3() {
		return OriginatorsSundryInformation3;
	}


	public void setOriginatorsSundryInformation3(String originatorsSundryInformation3) {
		OriginatorsSundryInformation3 = originatorsSundryInformation3;
	}


	public String getOrignDirectClearerUserID3() {
		return OrignDirectClearerUserID3;
	}


	public void setOrignDirectClearerUserID3(String orignDirectClearerUserID3) {
		OrignDirectClearerUserID3 = orignDirectClearerUserID3;
	}


	public String getInstitutionIdentificationNumber3() {
		return InstitutionIdentificationNumber3;
	}


	public void setInstitutionIdentificationNumber3(String institutionIdentificationNumber3) {
		InstitutionIdentificationNumber3 = institutionIdentificationNumber3;
	}


	public String getFiller3() {
		return Filler3;
	}


	public void setFiller3(String filler3) {
		Filler3 = filler3;
	}


	public String getTxnTypeCode4() {
		return TxnTypeCode4;
	}


	public void setTxnTypeCode4(String txnTypeCode4) {
		TxnTypeCode4 = txnTypeCode4;
	}


	public String getDateDundsAvl4() {
		return DateDundsAvl4;
	}


	public void setDateDundsAvl4(String dateDundsAvl4) {
		DateDundsAvl4 = dateDundsAvl4;
	}


	public String getBeneBankTranistNum4() {
		return BeneBankTranistNum4;
	}


	public void setBeneBankTranistNum4(String beneBankTranistNum4) {
		BeneBankTranistNum4 = beneBankTranistNum4;
	}


	public String getItemTracenumber4() {
		return ItemTracenumber4;
	}


	public void setItemTracenumber4(String itemTracenumber4) {
		ItemTracenumber4 = itemTracenumber4;
	}


	public String getStoredTxnType4() {
		return StoredTxnType4;
	}


	public void setStoredTxnType4(String storedTxnType4) {
		StoredTxnType4 = storedTxnType4;
	}


	public String getOriginatorsShortName4() {
		return OriginatorsShortName4;
	}


	public void setOriginatorsShortName4(String originatorsShortName4) {
		OriginatorsShortName4 = originatorsShortName4;
	}


	public String getOriginatorsLongName4() {
		return OriginatorsLongName4;
	}


	public void setOriginatorsLongName4(String originatorsLongName4) {
		OriginatorsLongName4 = originatorsLongName4;
	}


	public String getTransitNoReturns4() {
		return TransitNoReturns4;
	}


	public void setTransitNoReturns4(String transitNoReturns4) {
		TransitNoReturns4 = transitNoReturns4;
	}


	public String getAccNoReturns4() {
		return AccNoReturns4;
	}


	public void setAccNoReturns4(String accNoReturns4) {
		AccNoReturns4 = accNoReturns4;
	}


	public String getOrigSettlCode4() {
		return OrigSettlCode4;
	}


	public void setOrigSettlCode4(String origSettlCode4) {
		OrigSettlCode4 = origSettlCode4;
	}


	public String getInvalidDataElementID4() {
		return InvalidDataElementID4;
	}


	public void setInvalidDataElementID4(String invalidDataElementID4) {
		InvalidDataElementID4 = invalidDataElementID4;
	}


	public String getOriginatorsSundryInformation4() {
		return OriginatorsSundryInformation4;
	}


	public void setOriginatorsSundryInformation4(String originatorsSundryInformation4) {
		OriginatorsSundryInformation4 = originatorsSundryInformation4;
	}


	public String getOrignDirectClearerUserID4() {
		return OrignDirectClearerUserID4;
	}


	public void setOrignDirectClearerUserID4(String orignDirectClearerUserID4) {
		OrignDirectClearerUserID4 = orignDirectClearerUserID4;
	}


	public String getInstitutionIdentificationNumber4() {
		return InstitutionIdentificationNumber4;
	}


	public void setInstitutionIdentificationNumber4(String institutionIdentificationNumber4) {
		InstitutionIdentificationNumber4 = institutionIdentificationNumber4;
	}


	public String getFiller4() {
		return Filler4;
	}


	public void setFiller4(String filler4) {
		Filler4 = filler4;
	}


	public String getTxnTypeCode5() {
		return TxnTypeCode5;
	}


	public void setTxnTypeCode5(String txnTypeCode5) {
		TxnTypeCode5 = txnTypeCode5;
	}


	public String getDateDundsAvl5() {
		return DateDundsAvl5;
	}


	public void setDateDundsAvl5(String dateDundsAvl5) {
		DateDundsAvl5 = dateDundsAvl5;
	}


	public String getBeneBankTranistNum5() {
		return BeneBankTranistNum5;
	}


	public void setBeneBankTranistNum5(String beneBankTranistNum5) {
		BeneBankTranistNum5 = beneBankTranistNum5;
	}


	public String getItemTracenumber5() {
		return ItemTracenumber5;
	}


	public void setItemTracenumber5(String itemTracenumber5) {
		ItemTracenumber5 = itemTracenumber5;
	}


	public String getStoredTxnType5() {
		return StoredTxnType5;
	}


	public void setStoredTxnType5(String storedTxnType5) {
		StoredTxnType5 = storedTxnType5;
	}


	public String getOriginatorsShortName5() {
		return OriginatorsShortName5;
	}


	public void setOriginatorsShortName5(String originatorsShortName5) {
		OriginatorsShortName5 = originatorsShortName5;
	}


	public String getOriginatorsLongName5() {
		return OriginatorsLongName5;
	}


	public void setOriginatorsLongName5(String originatorsLongName5) {
		OriginatorsLongName5 = originatorsLongName5;
	}


	public String getTransitNoReturns5() {
		return TransitNoReturns5;
	}


	public void setTransitNoReturns5(String transitNoReturns5) {
		TransitNoReturns5 = transitNoReturns5;
	}


	public String getAccNoReturns5() {
		return AccNoReturns5;
	}


	public void setAccNoReturns5(String accNoReturns5) {
		AccNoReturns5 = accNoReturns5;
	}


	public String getOrigSettlCode5() {
		return OrigSettlCode5;
	}


	public void setOrigSettlCode5(String origSettlCode5) {
		OrigSettlCode5 = origSettlCode5;
	}


	public String getInvalidDataElementID5() {
		return InvalidDataElementID5;
	}


	public void setInvalidDataElementID5(String invalidDataElementID5) {
		InvalidDataElementID5 = invalidDataElementID5;
	}


	public String getOriginatorsSundryInformation5() {
		return OriginatorsSundryInformation5;
	}


	public void setOriginatorsSundryInformation5(String originatorsSundryInformation5) {
		OriginatorsSundryInformation5 = originatorsSundryInformation5;
	}


	public String getOrignDirectClearerUserID5() {
		return OrignDirectClearerUserID5;
	}


	public void setOrignDirectClearerUserID5(String orignDirectClearerUserID5) {
		OrignDirectClearerUserID5 = orignDirectClearerUserID5;
	}


	public String getInstitutionIdentificationNumber5() {
		return InstitutionIdentificationNumber5;
	}


	public void setInstitutionIdentificationNumber5(String institutionIdentificationNumber5) {
		InstitutionIdentificationNumber5 = institutionIdentificationNumber5;
	}


	public String getFiller5() {
		return Filler5;
	}


	public void setFiller5(String filler5) {
		Filler5 = filler5;
	}


	public String getTxnTypeCode6() {
		return TxnTypeCode6;
	}


	public void setTxnTypeCode6(String txnTypeCode6) {
		TxnTypeCode6 = txnTypeCode6;
	}


	public String getDateDundsAvl6() {
		return DateDundsAvl6;
	}


	public void setDateDundsAvl6(String dateDundsAvl6) {
		DateDundsAvl6 = dateDundsAvl6;
	}


	public String getBeneBankTranistNum6() {
		return BeneBankTranistNum6;
	}


	public void setBeneBankTranistNum6(String beneBankTranistNum6) {
		BeneBankTranistNum6 = beneBankTranistNum6;
	}


	public String getItemTracenumber6() {
		return ItemTracenumber6;
	}


	public void setItemTracenumber6(String itemTracenumber6) {
		ItemTracenumber6 = itemTracenumber6;
	}


	public String getStoredTxnType6() {
		return StoredTxnType6;
	}


	public void setStoredTxnType6(String storedTxnType6) {
		StoredTxnType6 = storedTxnType6;
	}


	public String getOriginatorsShortName6() {
		return OriginatorsShortName6;
	}


	public void setOriginatorsShortName6(String originatorsShortName6) {
		OriginatorsShortName6 = originatorsShortName6;
	}


	public String getOriginatorsLongName6() {
		return OriginatorsLongName6;
	}


	public void setOriginatorsLongName6(String originatorsLongName6) {
		OriginatorsLongName6 = originatorsLongName6;
	}


	public String getTransitNoReturns6() {
		return TransitNoReturns6;
	}


	public void setTransitNoReturns6(String transitNoReturns6) {
		TransitNoReturns6 = transitNoReturns6;
	}


	public String getAccNoReturns6() {
		return AccNoReturns6;
	}


	public void setAccNoReturns6(String accNoReturns6) {
		AccNoReturns6 = accNoReturns6;
	}


	public String getOrigSettlCode6() {
		return OrigSettlCode6;
	}


	public void setOrigSettlCode6(String origSettlCode6) {
		OrigSettlCode6 = origSettlCode6;
	}


	public String getInvalidDataElementID6() {
		return InvalidDataElementID6;
	}


	public void setInvalidDataElementID6(String invalidDataElementID6) {
		InvalidDataElementID6 = invalidDataElementID6;
	}


	public String getOriginatorsSundryInformation6() {
		return OriginatorsSundryInformation6;
	}


	public void setOriginatorsSundryInformation6(String originatorsSundryInformation6) {
		OriginatorsSundryInformation6 = originatorsSundryInformation6;
	}


	public String getOrignDirectClearerUserID6() {
		return OrignDirectClearerUserID6;
	}


	public void setOrignDirectClearerUserID6(String orignDirectClearerUserID6) {
		OrignDirectClearerUserID6 = orignDirectClearerUserID6;
	}


	public String getInstitutionIdentificationNumber6() {
		return InstitutionIdentificationNumber6;
	}


	public void setInstitutionIdentificationNumber6(String institutionIdentificationNumber6) {
		InstitutionIdentificationNumber6 = institutionIdentificationNumber6;
	}


	public String getFiller6() {
		return Filler6;
	}


	public void setFiller6(String filler6) {
		Filler6 = filler6;
	}


	public Utility getU() {
		return u;
	}


	public void setU(Utility u) {
		this.u = u;
	}


	public String getFiller() {
		return Filler;
	}


	public void setFiller(String filler) {
		Filler = filler;
	}


	public String getInstitutionIdentificationNumber() {
		return InstitutionIdentificationNumber;
	}


	public void setInstitutionIdentificationNumber(String institutionIdentificationNumber) {
		InstitutionIdentificationNumber = institutionIdentificationNumber;
	}




	public String getPayeeAccnumber() {
		return PayeeAccnumber;
	}


	public void setPayeeAccnumber(String payeeAccnumber) {
		PayeeAccnumber = payeeAccnumber;
	}


	public String getAmount() {
		return Amount;
	}


	public void setAmount(String amount) {
		Amount = amount;
	}


	public String getCrossrefno() {
		return Crossrefno;
	}


	public void setCrossrefno(String crossrefno) {
		Crossrefno = crossrefno;
	}


	public String getPayeeName() {
		return PayeeName;
	}


	public void setPayeeName(String payeeName) {
		PayeeName = payeeName;
	}


	public String getLogicalRecordTypeID() {
		return LogicalRecordTypeID;
	}


	public void setLogicalRecordTypeID(String logicalRecordTypeID) {
		LogicalRecordTypeID = logicalRecordTypeID;
	}


	public String getLogicalRecordCount() {
		return LogicalRecordCount;
	}


	public void setLogicalRecordCount(String logicalRecordCount) {
		LogicalRecordCount = logicalRecordCount;
	}


	public String getOriginationControlData() {
		return OriginationControlData;
	}


	public void setOriginationControlData(String originationControlData) {
		OriginationControlData = originationControlData;
	}


	public String getTxnTypeCode() {
		return TxnTypeCode;
	}


	public void setTxnTypeCode(String txnTypeCode) {
		TxnTypeCode = txnTypeCode;
	}


	public String getDateDundsAvl() {
		return DateDundsAvl;
	}


	public void setDateDundsAvl(String dateDundsAvl) {
		DateDundsAvl = dateDundsAvl;
	}


	public String getBeneBankTranistNum() {
		return BeneBankTranistNum;
	}


	public void setBeneBankTranistNum(String beneBankTranistNum) {
		BeneBankTranistNum = beneBankTranistNum;
	}


	public String getItemTracenumber() {
		return ItemTracenumber;
	}


	public void setItemTracenumber(String itemTracenumber) {
		ItemTracenumber = itemTracenumber;
	}


	public String getStoredTxnType() {
		return StoredTxnType;
	}


	public void setStoredTxnType(String storedTxnType) {
		StoredTxnType = storedTxnType;
	}


	public String getOriginatorsShortName() {
		return OriginatorsShortName;
	}


	public void setOriginatorsShortName(String originatorsShortName) {
		OriginatorsShortName = originatorsShortName;
	}


	public String getOriginatorsLongName() {
		return OriginatorsLongName;
	}


	public void setOriginatorsLongName(String originatorsLongName) {
		OriginatorsLongName = originatorsLongName;
	}


	public String getTransitNoReturns() {
		return TransitNoReturns;
	}


	public void setTransitNoReturns(String transitNoReturns) {
		TransitNoReturns = transitNoReturns;
	}


	public String getAccNoReturns() {
		return AccNoReturns;
	}


	public void setAccNoReturns(String accNoReturns) {
		AccNoReturns = accNoReturns;
	}


	public String getOrigSettlCode() {
		return OrigSettlCode;
	}


	public void setOrigSettlCode(String origSettlCode) {
		OrigSettlCode = origSettlCode;
	}


	public String getInvalidDataElementID() {
		return InvalidDataElementID;
	}


	public void setInvalidDataElementID(String invalidDataElementID) {
		InvalidDataElementID = invalidDataElementID;
	}


	public String getOriginatorsSundryInformation() {
		return OriginatorsSundryInformation;
	}


	public void setOriginatorsSundryInformation(String originatorsSundryInformation) {
		OriginatorsSundryInformation = originatorsSundryInformation;
	}


	public String getOrignDirectClearerUserID() {
		return OrignDirectClearerUserID;
	}


	public void setOrignDirectClearerUserID(String orignDirectClearerUserID) {
		OrignDirectClearerUserID = orignDirectClearerUserID;
	}
	public String getPayeeAccnumber2() {
		return PayeeAccnumber2;
	}


	public void setPayeeAccnumber2(String payeeAccnumber2) {
		PayeeAccnumber2 = payeeAccnumber2;
	}


	public String getAmount2() {
		return Amount2;
	}


	public void setAmount2(String amount2) {
		Amount2 = amount2;
	}


	public String getCrossrefno2() {
		return Crossrefno2;
	}


	public void setCrossrefno2(String crossrefno2) {
		Crossrefno2 = crossrefno2;
	}


	public String getPayeeName2() {
		return PayeeName2;
	}


	public void setPayeeName2(String payeeName2) {
		PayeeName2 = payeeName2;
	}


	public String getPayeeAccnumber3() {
		return PayeeAccnumber3;
	}


	public void setPayeeAccnumber3(String payeeAccnumber3) {
		PayeeAccnumber3 = payeeAccnumber3;
	}


	public String getAmount3() {
		return Amount3;
	}


	public void setAmount3(String amount3) {
		Amount3 = amount3;
	}


	public String getCrossrefno3() {
		return Crossrefno3;
	}


	public void setCrossrefno3(String crossrefno3) {
		Crossrefno3 = crossrefno3;
	}


	public String getPayeeName3() {
		return PayeeName3;
	}


	public void setPayeeName3(String payeeName3) {
		PayeeName3 = payeeName3;
	}


	public String getPayeeAccnumber4() {
		return PayeeAccnumber4;
	}


	public void setPayeeAccnumber4(String payeeAccnumber4) {
		PayeeAccnumber4 = payeeAccnumber4;
	}


	public String getAmount4() {
		return Amount4;
	}


	public void setAmount4(String amount4) {
		Amount4 = amount4;
	}


	public String getCrossrefno4() {
		return Crossrefno4;
	}


	public void setCrossrefno4(String crossrefno4) {
		Crossrefno4 = crossrefno4;
	}


	public String getPayeeName4() {
		return PayeeName4;
	}


	public void setPayeeName4(String payeeName4) {
		PayeeName4 = payeeName4;
	}


	public String getPayeeAccnumber5() {
		return PayeeAccnumber5;
	}


	public void setPayeeAccnumber5(String payeeAccnumber5) {
		PayeeAccnumber5 = payeeAccnumber5;
	}


	public String getAmount5() {
		return Amount5;
	}


	public void setAmount5(String amount5) {
		Amount5 = amount5;
	}


	public String getCrossrefno5() {
		return Crossrefno5;
	}


	public void setCrossrefno5(String crossrefno5) {
		Crossrefno5 = crossrefno5;
	}


	public String getPayeeName5() {
		return PayeeName5;
	}


	public void setPayeeName5(String payeeName5) {
		PayeeName5 = payeeName5;
	}


	public String getPayeeAccnumber6() {
		return PayeeAccnumber6;
	}


	public void setPayeeAccnumber6(String payeeAccnumber6) {
		PayeeAccnumber6 = payeeAccnumber6;
	}


	public String getAmount6() {
		return Amount6;
	}


	public void setAmount6(String amount6) {
		Amount6 = amount6;
	}


	public String getCrossrefno6() {
		return Crossrefno6;
	}


	public void setCrossrefno6(String crossrefno6) {
		Crossrefno6 = crossrefno6;
	}


	public String getPayeeName6() {
		return PayeeName6;
	}


	public void setPayeeName6(String payeeName6) {
		PayeeName6 = payeeName6;
	}
	public static void main(String[] args) throws IOException {
	    FileInputStream file = new FileInputStream(new File("D:\\CWB\\CWB_FGT.xlsx"));
	    XSSFWorkbook workbook = new XSSFWorkbook(file);
	    XSSFSheet sheet = workbook.getSheetAt(0);
	    Row row1 = sheet.getRow(49);
	    String segment2;
	    try
	    {
		segment2=(row1.getCell(240).toString());
		//System.out.println(segment2);
		segment2=(row1.getCell(240)).getStringCellValue();
		//System.out.println(":"+segment2);
		if((segment2.length()>0))
		{
		//	System.out.println(segment2);
		}
	    }
	    catch(Exception e)
	    {
	    
	    }
	
		
	}






	public String readtransactiondataNACHAEFT(Row row1, CPA005 c) {
		// TODO Auto-generated method stub
		Transaction t= new Transaction();
		t.setLogicalRecordTypeID((StringUtils.rightPad(row1.getCell(93).toString(),1)));
		t.setLogicalRecordCount((StringUtils.rightPad(row1.getCell(107).toString(),2)));//creditdebitidetifier txn code
		//goidentification
		t.setInstitutionIdentificationNumber((StringUtils.rightPad(row1.getCell(109).toString(),8)));
		//checkdigit
		t.setFiller2((StringUtils.rightPad(row1.getCell(124).toString(),1)));
		//receiver account number
		t.setPayeeAccnumber((StringUtils.rightPad(row1.getCell(96).toString(),17)));
		t.setAmount((StringUtils.leftPad(row1.getCell(101).toString(),10)));
		//IDENTIFICATION NUMBER
		t.setItemTracenumber((StringUtils.rightPad(row1.getCell(110).toString(),15)));
		//RECEIVING COMPANY NAME
		t.setPayeeName((StringUtils.rightPad(row1.getCell(104).toString(),22)));
		//Discretionary Data
		t.setFiller4((StringUtils.rightPad("",2)));
		//addenda record indicator
		t.setFiller5((StringUtils.rightPad(row1.getCell(125).toString(),1)));
		//cross refno
		t.setCrossrefno((StringUtils.rightPad(row1.getCell(102).toString(),22)));
		StringBuffer s=new StringBuffer(t.getLogicalRecordTypeID()).append(t.getLogicalRecordCount()).append(t.getInstitutionIdentificationNumber()).
		append(t.getFiller2()).append(t.getPayeeAccnumber()).append(t.getAmount()).append(t.getItemTracenumber())
		.append(t.getPayeeName()).append(t.getFiller4()).append(t.getFiller5())
		.append(t.getCrossrefno());
		
		
		return s.toString();
	}
	public String readtransactionaddendaNACHAEFT(Row row1, CPA005 c) {
		// TODO Auto-generated method stub
		Transaction t= new Transaction();
		t.setRecordTypeCode((StringUtils.rightPad(row1.getCell(127).toString(),1)));
		t.setAddendaTypecode((StringUtils.rightPad(row1.getCell(128).toString(),2)));
		t.setPaymentInfo((StringUtils.rightPad(row1.getCell(129).toString(),80)));
		t.setAddendaSeqno((StringUtils.rightPad(row1.getCell(130).toString(),4)));
		t.setEntryDetailSeqnumber((StringUtils.rightPad(row1.getCell(131).toString(),7)));
		StringBuffer s=new StringBuffer(t.getRecordTypeCode()).append(t.getAddendaTypecode()).append(t.getPaymentInfo()).
		append(t.getAddendaSeqno()).append(t.getEntryDetailSeqnumber());
		
		
		return s.toString();
	}






	public String getRecordTypeCode() {
		return RecordTypeCode;
	}






	public void setRecordTypeCode(String recordTypeCode) {
		RecordTypeCode = recordTypeCode;
	}






	public String getAddendaTypecode() {
		return AddendaTypecode;
	}






	public void setAddendaTypecode(String addendaTypecode) {
		AddendaTypecode = addendaTypecode;
	}






	public String getPaymentInfo() {
		return PaymentInfo;
	}






	public void setPaymentInfo(String paymentInfo) {
		PaymentInfo = paymentInfo;
	}






	public String getAddendaSeqno() {
		return AddendaSeqno;
	}






	public void setAddendaSeqno(String addendaSeqno) {
		AddendaSeqno = addendaSeqno;
	}






	public String getEntryDetailSeqnumber() {
		return EntryDetailSeqnumber;
	}






	public void setEntryDetailSeqnumber(String entryDetailSeqnumber) {
		EntryDetailSeqnumber = entryDetailSeqnumber;
	}
	}


