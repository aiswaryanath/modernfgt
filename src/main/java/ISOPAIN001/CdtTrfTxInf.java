package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CdtTrfTxInf {
	@JacksonXmlProperty
	PmtId PmtId;
	@JacksonXmlProperty
	PmtTpInf PmtTpInf;
	@JacksonXmlProperty
	Amt Amt;
	@JacksonXmlProperty
	XchgRateInf XchgRateInf;
	@JacksonXmlProperty
	IntrmyAgt1 IntrmyAgt1;
	@JacksonXmlProperty
	IntrmyAgt1Acct IntrmyAgt1Acct;
	@JacksonXmlProperty
	IntrmyAgt2 IntrmyAgt2;
	@JacksonXmlProperty
	IntrmyAgt2Acct IntrmyAgt2Acct;
	@JacksonXmlProperty
	
	IntrmyAgt3 IntrmyAgt3;
	@JacksonXmlProperty
	IntrmyAgt3Acct IntrmyAgt3Acct;
	@JacksonXmlProperty
	CdtrAgt CdtrAgt;
	@JacksonXmlProperty
	CdtrAgtAcct CdtrAgtAcct;
	@JacksonXmlProperty
	CdtrAcct CdtrAcct;
	@JacksonXmlProperty
	UltmtCdtr UltmtCdtr;
	@JacksonXmlProperty
	InstrForCdtrAgt InstrForCdtrAgt;
	@JacksonXmlProperty
	RltdRmtInf RltdRmtInf;
	@JacksonXmlProperty
	RmtInf RmtInf;


}
