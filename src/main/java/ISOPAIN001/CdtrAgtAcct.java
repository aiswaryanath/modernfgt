package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CdtrAgtAcct {
	@JacksonXmlProperty
	Id Id;
	@JacksonXmlProperty
	Tp Tp;
	@JacksonXmlProperty
	String Ccy;
	@JacksonXmlProperty
	String Nm;
}
