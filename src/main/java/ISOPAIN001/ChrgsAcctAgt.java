package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ChrgsAcctAgt {
	@JacksonXmlProperty
	FinInstnId FinInstnId;
	@JacksonXmlProperty
	BrnchId BrnchId;
}
