package ISOPAIN001;

import java.util.ArrayList;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CstmrCdtTrfInitn {
	@JacksonXmlProperty
	GrpHdr GrpHdr;
	@JacksonXmlElementWrapper
	ArrayList<PmtInf> PmtInf;
}
