package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CtctDtls {
	@JacksonXmlProperty
	String NmPrfx;
	@JacksonXmlProperty
	String Nm;
	@JacksonXmlProperty
	String PhneNb;
	@JacksonXmlProperty
	String MobNb;
	@JacksonXmlProperty
	String FaxNb;
	@JacksonXmlProperty
String EmailAdr;
	@JacksonXmlProperty
String Othr;
}
