package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class DbtrAgt {
	@JacksonXmlProperty
	PstlAdr PstlAdr;
	@JacksonXmlProperty
	FinInstnId FinInstnId;
	@JacksonXmlProperty
	BrnchId BrnchId;
}
