package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class GrpHdr {
	@JacksonXmlProperty
String MsgId;
	@JacksonXmlProperty
Authstn Authstn;
	@JacksonXmlProperty
String CreDtTm;
	@JacksonXmlProperty
String NbOfTxs;
	@JacksonXmlProperty
String CtrlSum;
	@JacksonXmlProperty
InitgPty InitgPty;
	@JacksonXmlProperty
FwdgAgt FwdgAgt;
}
