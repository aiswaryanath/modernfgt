package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class IntrmyAgt3Acct {
	@JacksonXmlProperty
	Id Id;
	@JacksonXmlProperty
	Tp Tp;
	@JacksonXmlProperty
	String Ccy;
	@JacksonXmlProperty
	String Nm;
}
