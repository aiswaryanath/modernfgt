package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "item")
public class ItemDTO {

    @JacksonXmlProperty(isAttribute = true)
    private String name = "Whatever";

    @JacksonXmlProperty
    private Problem problem = new Problem();
}