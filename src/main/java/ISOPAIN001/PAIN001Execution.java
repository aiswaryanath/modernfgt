package ISOPAIN001;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.codoid.products.exception.FilloException;

import Formats.CPA005;
import ModernFGT.ExcelData;
import ModernFGT.ExcelData2;
import ModernFGT.TestcaseName;

public class PAIN001Execution {


		public static HashMap<String,TestcaseName> datar=new HashMap<String,TestcaseName>();
		   static int counter=7;
		   static int rowstart=7;
		   static String Testname;
		   static String Testname1;
		   static String format;
		   static int batchno=1;
		   static HashMap<Integer,Integer> tb;
	    public static void main( String[] args ) throws FilloException, IOException, InterruptedException
	    {
		
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
			String dateAsISOString = df.format(date);
			File dir1 = new File("D:/CWB/Files");
			File dir2 = new File("D:/CWB/BACKUP/"+dateAsISOString);
			dir2.mkdir();
			FileUtils.copyDirectory(dir1,dir2);
			FileUtils.cleanDirectory(new File("D:/CWB/Files"));
	        FileInputStream file = new FileInputStream(new File("D:\\CWB\\Sanity_FGT_2.0.xlsx"));

		    XSSFWorkbook workbook = new XSSFWorkbook(file);
		    XSSFSheet sheet = workbook.getSheetAt(1);
		    int rowcount = sheet.getLastRowNum();
		    System.out.println("rowcount:"+rowcount);
		
		    TestcaseName tn=new TestcaseName();
			String flag="Y";
	        for (int i = 7; i < rowcount+1; i++) {	
	        	 
	            TestcaseName tn1=new TestcaseName();
				Row row1 = sheet.getRow(i);
				//System.out.println(row1.getCell(0).toString());
				 Testname = row1.getCell(0).toString();
				
				String txncount= row1.getCell(16).toString().substring(1);
				int Batchcount=1;
				try
				{
				Batchcount=Integer.valueOf(row1.getCell(15).toString().substring(1));
				}
				catch(Exception e)
				{
					
				}
				counter=Integer.valueOf(txncount);
			   
			   System.out.println(row1.getCell(1030).toString());
			
				//System.out.println("Txncount:"+txncount+"  Batch:"+Batchcount);
			
				if(counter>1)
				{
					tn1.name=Testname1;
					tn1.endrownum=i;
					tn1.startrownum=tn.startrownum;
				
					tb.put(counter,Batchcount);
					tn1.txnbatch=tb;
					if(flag.equals("Y"))
					{
					datar.put(Testname1,tn1);
					}
				}
				else
				{ 
					rowstart=i;
					tn1.startrownum=i;
					tn1.endrownum=i;
					tb=new HashMap<Integer,Integer>();
					tb.put(counter,Batchcount);
					tn1.txnbatch=tb;
					tn=tn1;
					Testname1=Testname;
					tn1.name=Testname;
					format=row1.getCell(2).toString();
				
					flag=row1.getCell(152).toString();
					//System.out.println("first record:"+Testname+":"+format);
					if(flag.equals("Y"))
					{
				datar.put(Testname,tn1);
					}
			
				}
	        }

	        for(Entry<String,TestcaseName> in : datar.entrySet())
	        {
	        System.out.println(in.getKey()+":"+"startrow:"+in.getValue().startrownum+":"+"endrow:"+in.getValue().endrownum);
	        tb= in.getValue().txnbatch;
	        for(Entry<Integer,Integer> txb : tb.entrySet())
	        {
	        	 System.out.println("txncount:"+txb.getKey()+":"+"batchcount:"+txb.getValue());
	        }
	        }
	        ExcelData d=new ExcelData();
	      
	      //  CPA005 c1=new CPA005();
	    //   c1=d.readExcelsheet(sheet,datar,c1);
	       
	       
	        

	        ExcelData2 d2=new ExcelData2();
	        CPA005 cv=new CPA005();
	        List<TestcaseName> l2=new ArrayList<TestcaseName>(datar.values());
	        for(TestcaseName tr1:l2)
	        {	
	      // d2.readExcelsheet(sheet, tr1, cv);
	        }
	        
	       
	       
	  /*
	       l2.parallelStream().map(x -> {
				try {
					d2.readExcelsheet(sheet,x,cv);
				} catch (IOException e) {
				
				System.out.println("Exception :::");
			
				}
				return cv;
			}).collect(Collectors.toList());
	       
	   
	       FileWriter fw=new FileWriter("D:\\CWB\\Files\\"+"mapping.txt");    					
	       for(Entry<String,TestcaseName> in : datar.entrySet())
	       {
	       System.out.println(in.getValue().toString());
	   	fw.write(in.getValue().toString()+"\n");
	       }
	       fw.flush();
				fw.close();
	        file.close();
	        System.gc();
	           */ 
	    }


}
