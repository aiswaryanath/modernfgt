package ISOPAIN001;


	import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.omg.PortableInterceptor.USER_EXCEPTION;

import com.fasterxml.jackson.dataformat.xml.*;
	public class POJOToXmlTest {
	   public static void main(String args[]) throws Exception {
	      try {
	         XmlMapper xmlMapper = new XmlMapper();
	         Person pojo = new Person();
	         pojo.setFirstName("Raja");
	         pojo.setLastName("Ramesh");
	         pojo.setAddress("Hyderabad");
	       //  ItemDTO pojo=new ItemDTO();

	         StringWriter writer = new StringWriter();
	         String xml = xmlMapper.writeValueAsString(pojo);
	         xml=getPrettyString(xml);
			   writer.write("<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03\" xsi:schemaLocation=\"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 file:///C:/Users/IBM_ADMIN/Box%20Sync/My%20Files/PSH/pain.001.001.03.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"+"\n"+xml+"</Document>");

	         System.out.println(writer.toString());
	        marshaling2(pojo);
	         
	      } catch(Exception e) {
	         e.printStackTrace();
	      }
	      
	   }
	   private static String marshaling2(Object object) throws JAXBException, XMLStreamException {
		    JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
		    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		    StringWriter writer = new StringWriter();
		    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		    jaxbMarshaller.marshal(object, writer);
		    jaxbMarshaller.marshal(object, new File(System.getProperty("user.dir")+"\\sample.xml"));
		    System.out.println(writer.toString());
		    return writer.toString();
	}
	   
	   public static String getPrettyString(String xmlData) throws Exception {
		   TransformerFactory transformerFactory = TransformerFactory.newInstance();
		   transformerFactory.setAttribute("indent-number",4);

		   Transformer transformer = transformerFactory.newTransformer();
		   transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		   transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		   StringWriter stringWriter = new StringWriter();
		   StreamResult xmlOutput = new StreamResult(stringWriter);

		   Source xmlInput = new StreamSource(new StringReader(xmlData));
		   transformer.transform(xmlInput, xmlOutput);

		   return xmlOutput.getWriter().toString();
		  }
	   
	   public static String createXml (Object xml) throws Exception {
		      try {
		         XmlMapper xmlMapper = new XmlMapper();
		       
		         StringWriter writer = new StringWriter();
		         String xml1 = xmlMapper.writeValueAsString(xml);
		         xml1=getPrettyString(xml1);
		         xml1=removeemptytags(xml1);
				   writer.write("<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03\" xsi:schemaLocation=\"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 file:///C:/Users/IBM_ADMIN/Box%20Sync/My%20Files/PSH/pain.001.001.03.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"+"\n"+xml1+"</Document>");

		         System.out.println(writer.toString());
		        // marshaling2(pojo);
		         
		      } catch(Exception e) {
		         e.printStackTrace();
		      }
			return "succ";
		      
		   }
	   
	   public static String removeemptytags(String xml)
	   {
		   String[] patterns = new String[] {
			        // This will remove empty elements that look like <ElementName/>
			        "\\s*<\\w+/>", 
			        // This will remove empty elements that look like <ElementName></ElementName>
			        "\\s*<\\w+></\\w+>", 
			        // This will remove empty elements that look like 
			        // <ElementName>
			        // </ElementName>
			        "\\s*<\\w+>\n*\\s*</\\w+>"
			    };

			    for (String pattern : patterns) {
			        Matcher matcher = Pattern.compile(pattern).matcher(xml);
			        xml = matcher.replaceAll("");
			    }

			  //  System.out.println(xml);
			
		return xml;
		   
	   }
	   
	   
	}