package ISOPAIN001;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

@XmlRootElement
public class Person {
	private String firstName;
	   private String lastName;
	   private String address;
	   
	   
	   
	   
	  public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@JacksonXmlProperty(isAttribute = true)
	   private String ok = "yyy";
	   @JacksonXmlProperty(localName = "element4", isAttribute = true)
	    String element4="dndn";
	   
}
