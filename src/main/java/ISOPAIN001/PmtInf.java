package ISOPAIN001;

import java.util.ArrayList;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PmtInf {
	@JacksonXmlProperty
	String PmtInfId;
	@JacksonXmlProperty
	String PmtMtd;
	@JacksonXmlProperty
	String BtchBookg;
	@JacksonXmlProperty
	String NbOfTxs;
	@JacksonXmlProperty
	String CtrlSum;
	@JacksonXmlProperty
	String ReqdExctnDt;
	@JacksonXmlProperty
	String PoolgAdjstmntDt;
	@JacksonXmlProperty
	Dbtr 	Dbtr;
	@JacksonXmlProperty
	DbtrAcct DbtrAcct;
	@JacksonXmlProperty
	DbtrAgt DbtrAgt;
	@JacksonXmlProperty
	DbtrAgtAcct DbtrAgtAcct;
	@JacksonXmlProperty
	String InstrForDbtrAgt;
	@JacksonXmlProperty
	UltmtDbtr UltmtDbtr;
	@JacksonXmlProperty
	String ChrgBr;

	@JacksonXmlProperty
	ChrgsAcct ChrgsAcct;
	@JacksonXmlProperty
	ChrgsAcctAgt ChrgsAcctAgt;
	@JacksonXmlElementWrapper(localName = "CdtTrfTxInf")
	ArrayList<CdtTrfTxInf> CdtTrfTxInf;
	
	
}
