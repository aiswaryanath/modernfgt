package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PmtTpInf {
	@JacksonXmlProperty
	String InstrPrty;
	@JacksonXmlProperty
	SvcLvl SvcLvl;
	@JacksonXmlProperty
	LclInstrm LclInstrm ;
	@JacksonXmlProperty
	CtgyPurp CtgyPurp;
}
