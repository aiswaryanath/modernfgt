package ISOPAIN001;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PstlAdr {
	@JacksonXmlProperty
	String AdrTp;
	@JacksonXmlProperty
	String Dept;
	@JacksonXmlProperty
	String SubDept;
	@JacksonXmlProperty
String StrtNm;
	@JacksonXmlProperty
String BldgNb;
	@JacksonXmlProperty
String PstCd;
	@JacksonXmlProperty
String TwnNm;
	@JacksonXmlProperty
String CtrySubDvsn;
	@JacksonXmlProperty
String Ctry;
	@JacksonXmlProperty
String AdrLine1;
	@JacksonXmlProperty
String AdrLine2;
	@JacksonXmlProperty
String AdrLine3;
	@JacksonXmlProperty
String AdrLine4;
	@JacksonXmlProperty
String AdrLine5;
	@JacksonXmlProperty
String AdrLine6;
	@JacksonXmlProperty
String AdrLine7;
}
