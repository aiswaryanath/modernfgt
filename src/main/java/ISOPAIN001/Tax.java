package ISOPAIN001;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import Formats.Cdtr;
import Formats.Dbtr;
public class Tax {
	@JacksonXmlProperty
Cdtr Cdtr;
	@JacksonXmlProperty
Dbtr Dbtr;
	@JacksonXmlProperty
String AdmstnZn;
	@JacksonXmlProperty
String RefNb;
	@JacksonXmlProperty
String Mtd;
	@JacksonXmlProperty
TtlTaxblBaseAmt TtlTaxblBaseAmt;
	@JacksonXmlProperty
TtlTaxAmt TtlTaxAmt;
	@JacksonXmlProperty
String Dt;
	@JacksonXmlProperty
String SeqNb;
	@JacksonXmlProperty
Rcrd Rcrd;
}
