package ISOPAIN001;

import java.io.*;
import java.util.ArrayList;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class Test {
    public Test(){
        try {
            //java.lang.System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.xsltc.trax.TransformerFactoryImpl");

            DocumentBuilderFactory dbFactory;
            DocumentBuilder dBuilder;
            Document original = null;
            try {
                dbFactory = DocumentBuilderFactory.newInstance();
                dBuilder = dbFactory.newDocumentBuilder();
                original = dBuilder.parse(new InputSource(new InputStreamReader(new FileInputStream("xml_ko.xml"))));
            } catch (SAXException | IOException | ParserConfigurationException e) {
                e.printStackTrace();
            }
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            TransformerFactory tf = TransformerFactory.newInstance();
            //tf.setAttribute("indent-number", 2);
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "Yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(new DOMSource(original), xmlOutput);
            java.lang.System.out.println(xmlOutput.getWriter().toString());
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }
    }

    public static void main(String[] args) throws Exception{
       // new Test();
        POJOToXmlTest p=new POJOToXmlTest();
        Formats.Amt ami=new Formats.Amt();
        ami.Ccy="USD";
        ami.setValue("6000");
        EqvtAmt eq=new EqvtAmt();
        eq.Amt=(ami);
        eq.CcyOfTrf=("USD");
      
        Amt am=new Amt();
	      
        InstdAmt in=new InstdAmt();
         in.Ccy=("CAD");
         in.setValue("5000");
         am.EqvtAmt=eq;
         am.InstdAmt=in;
         
         //Address 
         
         Addr a=new Addr();
        a.StrtNm="pashupati";
        a.BldgNb="402";
        PmtInf pm= new PmtInf();
        pm.NbOfTxs="80";
        pm.PmtInfId="sajsaj";
        CdtTrfTxInf c1=new CdtTrfTxInf();
        //c1.ChrgBr="DEBT";
        CdtTrfTxInf c2=new CdtTrfTxInf();
      // c2.ChrgBr="TEGY";
        ArrayList<CdtTrfTxInf> aim=new ArrayList<CdtTrfTxInf>();
        aim.add(c2);
        aim.add(c1);
        pm.CdtTrfTxInf=aim;
        p.createXml(pm);
        
    }
}