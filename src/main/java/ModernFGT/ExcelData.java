package ModernFGT;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import Formats.CPA005;
import Formats.Header;
import Formats.Trailer;
import Formats.Transaction;

public class ExcelData {
static String raw;
HashMap<String,String> data=new HashMap<String,String>();
public  CPA005 readExcelsheet(XSSFSheet sheet,HashMap<String,TestcaseName> hm,CPA005 c) throws IOException, InterruptedException
{
    for(Entry<String,TestcaseName> in : hm.entrySet())
    {
    	int counter=1;
    	List<String> CPA=new ArrayList<String>();
    	 String Testname = "";
    	 raw="";
    	 for (int i = in.getValue().startrownum; i <=in.getValue().endrownum; i++) {	
    			
    		 Testname=in.getKey();
    		 //raw="";
    		 if(counter==1)
    		 {		
    			   System.out.println(Testname);
    			 Header h=new Header();
		Row row1 = sheet.getRow(i);
        String j= h.readheaderdata(row1,c);
        raw=raw+j+"\n";
        CPA.add(j);
     //System.out.println("H::"+j);
 	  String filename=getfilename(row1,c);
 	  c.setFilename(filename);
 	  in.getValue().setFilename(filename);
        Transaction t=new Transaction();
	
         String txn=t.readtransactiondata(row1,c);
        //System.out.println("T::"+txn);
         CPA.add(txn);
         raw=raw+txn+"\n";
	counter++;
    		 }
    		 else
    		 {
    			 if(i<in.getValue().endrownum)
    			 {	 
    			 Transaction t=new Transaction();
    				Row row1 = sheet.getRow(i);
    		         String txn=t.readtransactiondata(row1,c);
    		         //System.out.println("T::"+txn);
    		         CPA.add(txn);
    		         raw=raw+txn+"\n";
    			counter++;
    			 }
    			 else
    			 {
    				 Transaction t=new Transaction();
     				Row row1 = sheet.getRow(i);
     		         String txn=t.readtransactiondata(row1,c);
     		     //    System.out.println("T::"+txn);
     		         CPA.add(txn);
     		         raw=raw+txn+"\n";
     		
    				 Trailer t1=new Trailer();
     				
     			String tr= t1.readTrailerdata(row1);
     			//  System.out.println("F::"+tr);
     			  raw=raw+tr;
     			CPA.add(tr);
    			 }

    		 }
    		 
    		 if(i==in.getValue().startrownum && i==in.getValue().endrownum)
    		 {
    			 Trailer t=new Trailer();
    				Row row1 = sheet.getRow(i);
    			String tr= t.readTrailerdata(row1);
    			  //System.out.println("F::"+tr);
    			  raw=raw+tr;
    			CPA.add(tr);
    			//String finalwr=new String();
    		   // for(int i3=0;i3<CPA.size();i3++)
    	       // {
    	       // 	finalwr=(CPA.get(i3)+"\n");
    	        //	System.out.println("FINAL:"+finalwr);
    	       // }
    			//data.put(Testname,finalwr);
    			//c.setData(data);
    		 }

}
    	 //internal for loop
		String finalwr=new String();
	    for(int i=0;i<CPA.size();i++)
	      {
	       	finalwr=finalwr+(CPA.get(i)+"\n");
	        	
	     }
		    System.out.println("FINAL:"+Testname);
		    System.out.println(finalwr);
	//local
//FileWriter fw=new FileWriter(System.getProperty("user.dir")+"\\CWB\\Files\\"+c.getFilename());  
		    //production
		FileWriter fw=new FileWriter(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\Files\\"+c.getFilename()); 
			fw.write(finalwr);
			fw.flush();
			fw.close();
			data.put(Testname,raw);
			c.setData(data);
}
    //external for loop
    return c;
}

private String getfilename(Row row1, CPA005 c) throws InterruptedException {
	String Originator=row1.getCell(4).toString();
	String fcn=c.getHeader().getFileCreationNumber();
    String ChannelID=row1.getCell(3).toString();
    Thread.sleep(1000);
    String DateFormat=new Utility().getDateFormat("yyyyMMddHHmmss");
	String PTind=row1.getCell(13).toString();
	String ext=".1464."+PTind+".dat.tmp";

	ArrayList<String> CWB=new ArrayList<String>(Arrays.asList("H2HC","H2HB","DIGI"));
	ArrayList<String> CWB1=new ArrayList<String>(Arrays.asList("DATAELEC","DATAELUS"));
	StringBuilder filename=new StringBuilder(ChannelID).append(".").append(DateFormat).append(".").append(Originator).append(ext);
	if(CWB.contains(ChannelID))
	{
	filename=new StringBuilder(ChannelID).append(".").append(DateFormat).append(".").append(Originator).append(ext);
	}
	if(CWB1.contains(ChannelID))
	{
		String s=new Utility().getRandomnumber(3);
		filename=new StringBuilder(ChannelID).append(".").append(s).append(".").append(PTind).append(".tmp");
	}
	System.out.println(filename.toString());
	return filename.toString();
}

}