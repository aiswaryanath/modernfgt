package ModernFGT;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import Formats.CPA005;
import Formats.Header;
import Formats.Trailer;
import Formats.Transaction;

public class ExcelData2 {
static String raw;
HashMap<String,String> data=new HashMap<String,String>();
public  CPA005 readExcelsheet(XSSFSheet sheet,TestcaseName tr1,CPA005 c) throws IOException
{

    	int counter=1;
    	List<String> CPA=new ArrayList<String>();
    	 String Testname = "";
    	 raw="";
    	 for (int i = tr1.startrownum; i <=tr1.endrownum; i++) {	
    			
    		 Testname=tr1.name;
    		 //raw="";
    		 if(counter==1)
    		 {		
    			   System.out.println(Testname);
    			 Header h=new Header();
		Row row1 = sheet.getRow(i);
		 // String filename=getfilename(row1,c);
        String j= h.readheaderdata(row1, c);
        raw=raw+j+"\n";
        CPA.add(j);
      //System.out.println("H::"+j);
        Transaction t=new Transaction();
	
         String txn=t.readtransactiondata(row1,c);
        //System.out.println("T::"+txn);
         CPA.add(txn);
         raw=raw+txn+"\n";
	counter++;
	  String filename=getfilename(row1,c);
	  tr1.filename=filename;
	  c.setFilename(filename);
    		 }
    		 else
    		 {
    			 if(i<tr1.endrownum)
    			 {	 
    			 Transaction t=new Transaction();
    				Row row1 = sheet.getRow(i);
    		         String txn=t.readtransactiondata(row1,c);
    		         //System.out.println("T::"+txn);
    		         CPA.add(txn);
    		         raw=raw+txn+"\n";
    			counter++;
    			 }
    			 else
    			 {
    				 Transaction t=new Transaction();
     				Row row1 = sheet.getRow(i);
     		         String txn=t.readtransactiondata(row1,c);
     		       //  System.out.println("T::"+txn);
     		         CPA.add(txn);
     		         raw=raw+txn+"\n";
     		
    				 Trailer t1=new Trailer();
     				
     			String tr= t1.readTrailerdata(row1);
     			//  System.out.println("F::"+tr);
     			  raw=raw+tr;
     			CPA.add(tr);
    			 }

    		 }
    		 
    		 if(i==tr1.startrownum && i==tr1.endrownum)
    		 {
    			 Trailer t=new Trailer();
    				Row row1 = sheet.getRow(i);
    			String tr= t.readTrailerdata(row1);
   
    			  raw=raw+tr;
    			CPA.add(tr);

    			
    		 }

}
    	 //internal for loop
		String finalwr=new String();
	    for(int i=0;i<CPA.size();i++)
	      {
	       	finalwr=finalwr+(CPA.get(i)+"\n");
	        	
	     }
	    FileWriter fw=new FileWriter("D:\\CWB\\Files\\"+tr1.filename);    
	 			fw.write(finalwr);
	 			fw.flush();
	 			fw.close();
	
			data.put(Testname,raw);
			c.setData(data);
			c.setFinalstring(finalwr);

    //external for loop
    return c;
}
private String getfilename(Row row1, CPA005 c) {
	String Originator=row1.getCell(4).toString();
	String fcn=c.getHeader().getFileCreationNumber();
    String ChannelID=row1.getCell(3).toString();
    String DateFormat=new Utility().getDateFormat("yyyyMMddHHmmss");
	String PTind=row1.getCell(13).toString();
	String ext=".1464."+PTind+".dat.tmp";
	StringBuilder filename=new StringBuilder(ChannelID).append(".").append(DateFormat).append(".").append(Originator).append(ext);
	System.out.println(filename.toString());
	return filename.toString();
}
}