package ModernFGT;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Formats.CPA005;

import org.apache.commons.lang3.StringUtils;


/**
 * Hello world!
 *
 */
public class FGTEntry 
{
	public static HashMap<String,TestcaseName> datar=new HashMap<String,TestcaseName>();
	   static int counter=21;
	   static int rowstart=21;
	   static String Testname;
	   static String Testname1;
	   static String format;
	   static String folder;
    public static void main( String[] args ) throws FilloException, IOException, InterruptedException
    {
    	try
    	{
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
		String dateAsISOString = df.format(date);
		File dir1 = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\Files");
		File dir2 = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\BACKUP\\"+dateAsISOString);
		dir2.mkdir();
		FileUtils.copyDirectory(dir1,dir2);
		FileUtils.cleanDirectory(new File(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\Files"));
       // FileInputStream file = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\CWB_FGT.xlsx"));
		 FileInputStream file = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\"+args[0]));
	    XSSFWorkbook workbook = new XSSFWorkbook(file);
	    XSSFSheet sheet = workbook.getSheetAt(0);
	    int rowcount = sheet.getLastRowNum();
	    System.out.println("rowcount:"+rowcount);
	 
	    TestcaseName tn=new TestcaseName();
		String flag="Y";
        for (int i = 21; i < rowcount+1; i++) {	
        	 
            TestcaseName tn1=new TestcaseName();
			Row row1 = sheet.getRow(i);
			//System.out.println(row1.getCell(0).toString());
			 Testname = row1.getCell(0).toString();
			
			String txncount= row1.getCell(16).toString().substring(1);
			counter=Integer.valueOf(txncount);
		
		
		
			//System.out.println(Testname+":"+txncount);
		
			if(counter>1)
			{
				tn1.name=Testname1;
				tn1.endrownum=i;
				tn1.startrownum=tn.startrownum;
				tn1.folder=folder;
				tn1.format=format;
				if(flag.equals("Y"))
				{
				datar.put(Testname1,tn1);
				}
			}
			else
			{ 
				rowstart=i;
				tn1.startrownum=i;
				tn1.endrownum=i;
				tn=tn1;
				Testname1=Testname;
				tn1.name=Testname;
				folder=row1.getCell(3).toString();
				tn1.folder=(folder);
				format=row1.getCell(2).toString();
				tn1.format=(format);
				flag=row1.getCell(221).toString();
				//System.out.println("first record:"+Testname+":"+format);
				if(flag.equals("Y"))
				{
			datar.put(Testname,tn1);
				}
		
			}
        }
        HashMap<String,TestcaseName> cibc_80=new HashMap<String,TestcaseName>();
        HashMap<String,TestcaseName> nacha_eft=new HashMap<String,TestcaseName>();
        HashMap<String,TestcaseName> cpa005=new HashMap<String,TestcaseName>();
        for(Entry<String,TestcaseName> in : datar.entrySet())
        {
        System.out.println(in.getKey()+":"+"startrow:"+in.getValue().startrownum+":"+"endrow:"+in.getValue().endrownum+":"+"format:"+in.getValue().folder);
        if(in.getValue().format.equals("CPA005"))
        {
       	cpa005.put(in.getKey(),in.getValue());
        }
        if(in.getValue().format.equals("CIBC80"))
        {
        	cibc_80.put(in.getKey(),in.getValue());
        }
       if(in.getValue().format.equals("NACHA94"))
        {
        	nacha_eft.put(in.getKey(),in.getValue());
        }
        }
        ExcelData d=new ExcelData();
      
        CPA005 c1=new CPA005();
        if(cpa005.size()>0)
        {
        	c1=d.readExcelsheet(sheet,cpa005,c1);
        } 
      
      CIBC80_ExcelData c8=new CIBC80_ExcelData();
      if(cibc_80.size()>0)
      {  
    	  c1=c8.readExcelsheet(sheet,cibc_80,c1);
      }
        NACHA_EFT_ExcelData nj=new NACHA_EFT_ExcelData();
        if(nacha_eft.size()>0)
        {
        	c1=nj.readExcelsheet(sheet,nacha_eft, c1);
        }

          FileWriter fw=new FileWriter(System.getProperty("user.dir")+"\\src\\test\\resources\\CWB\\"+"mapping.txt");    					
       for(Entry<String,TestcaseName> in : datar.entrySet())
       {
       System.out.println(in.getValue().toString());
   	fw.write(in.getValue().toString()+"\n");
       }
       fw.flush();
			fw.close();
        file.close();
        System.out.println("FGT Executed successfully");
        System.gc();
    	}
    	catch(Exception e)
    	{
       e.printStackTrace();
    	}
        
    }

}
