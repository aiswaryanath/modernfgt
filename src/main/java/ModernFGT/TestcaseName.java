package ModernFGT;

import java.util.HashMap;

public class TestcaseName {
	public String name;
	public int startrownum;
	public int endrownum;
	public String filename;
	public String format;
	public String folder;
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public HashMap<Integer,Integer> txnbatch;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	/*
	@Override
	public String toString() {
		return "name=" + name + ",filename=" + filename + ",folder=" + folder;
	}
	*/
	@Override
	public String toString() {
		return "TestcaseName [name=" + name + ", filename=" + filename + ", folder=" + folder + ", format=" + format
				+ "]";
	}




}
