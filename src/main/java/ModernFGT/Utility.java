package ModernFGT;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

public class Utility {
   static int counter=1000;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Utility u=new Utility();
		String days="-9";
		
		u.getDateFormat("HHmm");
		/*
		u.getSeriesnumber(9);
		u.getSeriesnumber(9);
		u.getSeriesnumber(9);
	
		u.getISODate(days);
			*/
	}
	
	public String getISODate(String day)
	{
		int days=0;
		try
		{
		days =(int) (Double.parseDouble(day));
		}
		catch(Exception e)
		{
			days=0;
		}
		Calendar calendar = Calendar.getInstance();
		int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR); 
		dayOfYear=(days)+dayOfYear;
		int Year = calendar.get(Calendar.YEAR); 

		String ISODATE="0"+(Integer.toString(Year)).substring(2)+StringUtils.leftPad(Integer.toString(dayOfYear),3,'0');
	//	System.out.println(ISODATE);
		return ISODATE;
		
	}
	
	public static String getRandomnumber(int length)
	{
		String numeric="0123456789";
		int _NumLength=length;
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < _NumLength) { // length of the random string.
		    int index = (int) (rnd.nextFloat() * numeric.length());
		    salt.append(numeric.charAt(index));
		}
		String saltStr = salt.toString();
		//System.out.println(saltStr);
		
		
		return saltStr;
		
	}
	
	public static String getAlphabhet(int length)
	{
		String numeric="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int _NumLength=length;
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < _NumLength) { // length of the random string.
		    int index = (int) (rnd.nextFloat() * numeric.length());
		    salt.append(numeric.charAt(index));
		}
		String saltStr = salt.toString();
		//System.out.println(saltStr);
		
		
		return saltStr;
		
	}
	
	
	public static String getSeriesnumber(int length)
	{
		
		String numeric="00000";
		int _NumLength=length;
		StringBuilder salt = new StringBuilder();
	    counter++;
		String saltStr =StringUtils.leftPad(numeric+String.valueOf(counter),length,"0");
		//System.out.println(saltStr);
		
		
		return saltStr;
		
	}
	
	public String getDateFormat(String format)
	{
		
		Date date = new Date();
		DateFormat df = new SimpleDateFormat(format);
		String dateAsISOString = df.format(date);
		//System.out.println(dateAsISOString);
		return dateAsISOString;
	
		
	}

}
